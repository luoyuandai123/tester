# _*_coding:utf-8 _*_

from loggingSetting import *
import xlrd, json, time, requests, hashlib, openpyxl

logger = log_set()


def get_md5(src):
    '''
    传入字符串，转化为对应的md5值
    '''
    myMd5 = hashlib.md5()
    myMd5.update(src.encode("utf8"))
    myMd5_Digest = myMd5.hexdigest()
    return myMd5_Digest


def now_timestamp():
    '''
    获取当前10位时间戳
    '''
    now = int(time.time())
    return now


def now_time():
    '''
    获取当前时间，格式为%Y-%m-%d %H:%M:%S
    '''
    return str(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime()))


def openWorkbook(excelFile, column=1, row=2, sheet=0):
    '''
    读取excel，其中特殊处理了表内入参列的数据和测试结果列的数据
    column:填写key的取值行数(填写excel内的实际行数），针对用例表默认为1
    row：填写velue的取值行数(填写excel内的实际行数），针对用例表默认为2
    默认读取表里的第一个sheet的内容，如果要修改，则传对应excel里的sheet的数字（从0开始），默认为0
    '''

    # 读取excel表的数据
    # excelFile = u'F:\kleins_client\BuildDataConfig\DataConfig\Common\wp_物品信息\wp_物品信息.xlsx'
    workbook = xlrd.open_workbook(excelFile)

    # 选取需要读取数据的那一页
    sheet = workbook.sheet_by_index(int(sheet))
    # 获得行数和列数
    rows = sheet.nrows
    cols = sheet.ncols
    # 创建一个数组用来存储excel中的数据
    p = []
    row = int(row) - 1
    column = int(column) - 1
    for i in range(row, rows):
        d = {}
        for j in range(0, cols):
            if len('%s' % sheet.cell(column, j).value) == 0:
                pass
            else:
                q = '%s' % sheet.cell(column, j).value
                d[q] = sheet.cell(i, j).value
        ap = []
        for k, v in d.items():
            if k != '入参':
                if k == '测试结果':#因为会把测试结果写入到excel内，所以不需要拿这一部分数据来就行操作
                    pass
                else:
                    if isinstance(v, float):  # excel中的值默认是float,需要进行判断处理，通过'"%s":%d'，'"%s":"%s"'格式化数组
                        ap.append('"%s":%d' % (k, v))
                    else:
                        ap.append('"%s":"%s"' % (k, v))
            else:
                ap.append('"%s":%s' % (k, v))#解决入参为字典，导致下面的json.loads读取报错的问题
        s = '{%s}' % (','.join(ap))  # 继续格式化
        p.append(s)
    t = '[%s]' % (','.join(p))  # 格式化
    data = json.dumps(t, ensure_ascii=False)
    excel_json = data.replace('\\', '')[1:-1]
    try:
        x = json.loads(excel_json)
    except:
        logger.error(u'excel内json读取异常，请检查')
        x = -1
    workbook.release_resources()#释放掉excel
    return x


def data_write(excelPath, data,row,column):
    '''
    将传入的内容写入excel表内
    :param excelPath:文件路径
    :param data:入参
    :param row: 传入行数
    :param column: 传入列数
    :return:
    '''
    f = openpyxl.load_workbook(excelPath)
    ws = f['suite']
    # f = f.active

    ws.cell(row=row, column=column, value=str(data))
    try:
        f.save(excelPath)  # 保存文件
        logger.info(u'结果写入成功')
    except OSError as e:
        logger.error(u'写入测试结果失败，%s'%e)

def encryption(date,time):
    '''
    传入入参，入参将进行加密并且生成sign
    '''
    list = []
    # 将入参循环拆解成字符串
    date = json.dumps(date)
    date = eval(date)
    for key, value in sorted(date.items(), reverse=False):
        if key == 'sign':
            pass
        else:
            # 字符串重新拼接
            if key == 'timestamp':
                zz = "{}={}".format('timestamp', time)
                list.append(zz)
            else:
                zz = "{}={}".format(key, value)
                list.append(zz)
    # 将字符串和秘钥拼接，字段以&分隔
    AuthToken = '&'.join(list) + "XXX"#XX为项目秘钥key
    # 将入参加密
    sign = get_md5(AuthToken)
    # 加密后，将新的sign塞回入参内
    date['timestamp'] = time
    date['sign'] = sign
    return date


def reqapi(reqtype, host, api, token, date):
    '''
    根据传入的请求类型和其余请求参数去识别处理对应的请求，适用于读取excel的内容
    :param reqtype:请求类型：get/post/put等
    :param host:请求的地址
    :param api:请求的API接口地址
    :param token:如果是需要登录的接口，需要传入token
    :param date:传入dict格式的入参
    :return:
    '''
    s = requests.session()
    if token != 'None' or token != '':
        headers = {'x-token': token}
    else:
        headers = None
    try:
        if reqtype == 'GET':
            req = s.get(host + api + "?" + str(date), headers=headers, verify=False)
        elif reqtype == 'POST':
            time = now_timestamp()
            encdate = encryption(date, time)
            req = s.post(host + api, json=encdate)
        elif reqtype == 'PUT':
            req = s.put(host + api, data=json.dumps(date), headers=headers, verify=False)
        else:
            logger.error(u'%s为暂时不支持当前接口类型'%reqtype)
            return -1
        if req.json()['code'] == 0:
            logger.info(u'%s接口响应成功，返回：%s' % (api, req.json()))
            return req.json()
        else:
            logger.error(u'%s接口返回状态码不为0，响应为：%s' % (api,req.json()['code']))
            return req.json()
    except OSError as e:
        logger.error(u'%s接口error，返回：%s' % (api, e))
        return e




if __name__ == '__main__':

    try:
        test_suite_dict = openWorkbook(u'test_suite.xlsx')
        if test_suite_dict == -1:
            pass
        else:
            for i in range(len(test_suite_dict)):
                if test_suite_dict[i]["是否需要重复执行"] == '是' or test_suite_dict[i]["是否需要重复执行"] == '':
                    x = i + 2
                    y = 9
                    runnerstate = 10
                    report = reqapi(test_suite_dict[i]["请求类型(大写)"], test_suite_dict[i]["请求地址"],
                                    test_suite_dict[i]["API地址"], test_suite_dict[i]["token"], test_suite_dict[i]["入参"])
                    try:
                        if report['code'] == 0:
                            print(report)
                            data_write('test_suite.xlsx', report, x, y)
                            data_write('test_suite.xlsx', '否', x, runnerstate)
                        else:
                            data_write('test_suite.xlsx', report, x, y)
                            data_write('test_suite.xlsx', '是', x, runnerstate)
                    except:
                        data_write('test_suite.xlsx', report, x, y)
                        data_write('test_suite.xlsx', '是', x, runnerstate)
                else:
                    pass
    except OSError as e:
        logger.error(u'读取本地文件失败：%s' % e)


