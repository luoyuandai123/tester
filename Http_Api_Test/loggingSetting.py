#coding=utf-8
import logging,os,time
import colorlog
import sys,os

def log_set():
    log_colors_config = {
        'DEBUG': 'white',  # cyan white
        'INFO': 'white',
        'WARNING': 'yellow',
        'ERROR': 'red',
        'CRITICAL': 'bold_red',
    }

    logger = logging.getLogger('logger_name')
    now = time.strftime('%Y-%m-%d')
    # 输出到控制台
    console_handler = logging.StreamHandler()
    # 输出到文件
    file_handler = logging.FileHandler("logs/http_api_"+str(now)+".log")

    # 日志级别，logger 和 handler以最高级别为准，不同handler之间可以不一样，不相互影响
    logger.setLevel(logging.INFO)
    console_handler.setLevel(logging.INFO)
    file_handler.setLevel(logging.INFO)

    # 日志输出格式
    file_formatter = logging.Formatter(
        fmt='[%(asctime)s] %(filename)s->%(funcName)s line:%(lineno)d [%(levelname)s] : %(message)s',
        datefmt='%Y-%m-%d  %H:%M:%S'
    )
    console_formatter = colorlog.ColoredFormatter(
        fmt='%(log_color)s[%(asctime)s] %(filename)s->%(funcName)s line:%(lineno)d [%(levelname)s] : %(message)s',
        datefmt='%Y-%m-%d  %H:%M:%S',
        log_colors=log_colors_config
    )
    console_handler.setFormatter(console_formatter)
    file_handler.setFormatter(file_formatter)

    # 重复日志问题：
    # 1、防止多次addHandler；
    # 2、loggername 保证每次添加的时候不一样；
    # 3、显示完log之后调用removeHandler
    logger.addHandler(console_handler)
    logger.addHandler(file_handler)
    return logger