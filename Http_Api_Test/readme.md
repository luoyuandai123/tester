## 使用说明：

1、需要安装xlrd、requests、openpyxl库。

pip install xlrd -i <http://pypi.doubanio.com/simple/> --trusted-host pypi.doubanio.com

pip install requests -i <http://pypi.doubanio.com/simple/> --trusted-host pypi.doubanio.com

pip install openpyxl -i <http://pypi.doubanio.com/simple/> --trusted-host pypi.doubanio.com

2、按照test_suite.xlsx内的表头提示填写参数即可。

![1685266756083](F:\Test\Test_script\Http_Api_Test\%5CUsers%5CJOHN%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5C1685266756083.png)

3、双击run.bat运行程序。

4、脚本执行完成后，执行结果会回填到excel的'测试结果'列。

**注：打开excel状态下会导致测试结果写入失败，执行脚本前请关闭test_suite.xlsx文件**