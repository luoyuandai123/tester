# _*_coding:utf-8 _*_
from Bingkolo_Protobuf_Api_Test.base.base_api import *
from Bingkolo_Protobuf_Api_Test.module.cli_login import *
from Bingkolo_Protobuf_Api_Test.module.cli_guide import *
from Bingkolo_Protobuf_Api_Test.module.cli_chat import *
from Bingkolo_Protobuf_Api_Test.module.cli_room import *
from Bingkolo_Protobuf_Api_Test.module.cli_group import *

def login_yace(conn):
    '''
    登录的压测函数，传入socket连接
    :param conn:
    :return:
    '''
    userid = random_number()
    token = get_token('bingkolo', 'Win', userid, '11111111')
    loginreq = login_req(userid, token, 'bingkolo', env)
    loginreqmsg = presssend(1001, random_fournumber(), loginreq)
    loginres = login_res()
    start_time = time.time()
    Head, body = verifycmdid(conn, loginreqmsg, loginres, 1002)
    end_time = time.time()
    response_time = end_time - start_time
    logger.info('当前协议响应retcode为：%s,userid是：%s,请求响应时间为:%.4f 秒' % (Head.RetCode,userid,response_time))
    return Head.RetCode


def loginandcreatacter_yace(conn,userid):
    '''
    登录+创角的压测函数，传入socket连接
    :param conn:
    :return:
    '''
    # userid = random_number()

    token,account = get_token('bingkolo', 'Win', userid, '11111111')
    try:
        loginreq = login_req(userid, token, 'bingkolo', env,account)
        loginres = login_res()
        start_time = time.time()
        loginreqmsg = presssend(1001, random_fournumber(), loginreq)
        Head, body = verifycmdid(conn, loginreqmsg, loginres, 1002)
        if Head.RetCode == 7:
            # 发送创建角色协议
            createactorreq = createactor_req('我是%s'%userid[0-3])
            createactorreqmsg = presssend(1003, random_fournumber(), createactorreq)
            createactorres = createactor_res()
            # 收到创建角色协议汇回包
            Head, body = verifycmdid(conn, createactorreqmsg, createactorres, 1004)
            end_time = time.time()
            response_time = end_time - start_time
            if Head.RetCode == 0:
                logger.info('创角协议响应retcode为：%s,Accid是：%s,userid是：%s，从注册账号到创建角色成功花了:%.4f 秒' % (Head.RetCode,body.ActorData.AccId,userid,response_time))
            else:
                logger.info('创角协议响应retcode为：%s,userid是：%s，从注册账号到创建角色花了:%.4f 秒' % (Head.RetCode,userid,response_time))
        else:
            logger.info('登录协议响应retcode为：%s,userid是：%s' % (Head.RetCode, userid))
        return Head.RetCode
    except Exception as e:
        logger.info('请求都G了%s'%e)
        return -1

class MyThread(threading.Thread):
    '''
    压测专用类
    init内传入需要压测的target和args
    '''
    def __init__(self, target, args=None):
        threading.Thread.__init__(self)
        self.target = target
        self.args = args

    def run(self):
        self.result = self.target(*self.args)

    def get_result(self):
        return self.result


def get_reqresult(result_multi):
    '''
    计算响应内容的结果
    :param result_multi:
    :return:
    '''
    success = []
    failed = []
    error = []
    lastfailed = []
    for i in result_multi:
        if i == 0:
            success.append(i)
        elif i > 0:
            failed.append(i)
        else:
            error.append(i)
    try:
        logger.info('返回成功的请求数量为:%s，成功率:%.2f%%，返回失败的请求数量为:%s，失败率:%.2f%%，返回error的请求数量为:%s，error率:%.2f%%'%
                    (len(success),(len(success)/(len(success)+len(failed)+len(error)))*100,len(failed),(len(failed)/(len(success)+len(failed)+len(error)))*100, len(error),(len(error)/(len(success)+len(failed)+len(error)))*100))
        for x in failed:
            if x not in lastfailed:
                lastfailed.append(x)
            else:
                pass
        if len(failed) >0:
            c = Counter(failed)
            # logger.info('失败的错误主要是:%s' % lastfailed)
            total = sum(c.values())
            percentages = {k: v / total * 100 for k, v in c.items()}
            logger.info('不同错误码占总报错数量的比例：%s'%percentages)
        else:
            pass
    except:
        pass
    return success,failed,error,lastfailed

def multi_thread(xunhuan_num,bingfa_num,waittime,target):
    '''
    执行压测的方法
    :param xunhuan_num: 传入压测的执行循环次数
    :param bingfa_num: 传入压测的并发数量
    :param waittime: 传入压测时每个脚本的等待时间
    :return: 返回传入的target方法对应的return内容
    '''
    reportlist = []
    logging.info('开始执行......')
    for i in range(xunhuan_num):
        i += 1
        logger.info('==============================当前为第%s次执行=================================================' % i)
        socketlist = []
        useridlist = []
        try:
            for x in range(bingfa_num):
                sockconner = tcp_conn()
                socketlist.append(sockconner)
                useridlist.append(random_number())
        except Exception as e:
            logger.error('创建socket连接报错啦：%s' % e)
        threads = []
        if len(socketlist) >0 and len(socketlist) == bingfa_num:
            for i in range(bingfa_num):
                # logging.info('当前为第%s个数据' % i)
                # sockconnerr = tcp_conn()
                thread1 = MyThread(target=target,args=(socketlist[i],useridlist[i],))
                threads.append(thread1)
            for x in threads:
                time.sleep(waittime)
                x.start()
            for thread in threads:
                thread.join()
                reportlist.append(thread.get_result())
        else:
            logger.error('socket链接数量和压测并发数不一致，第%s次循环的执行失败，socket链接数为:%s，并发数为:%s'%(len(socketlist),i,bingfa_num))
    return reportlist  # 返回多线程返回的结果组成的列表

def create_tishenuser(conn,userid,device):
    '''
    创建提审账号
    :param conn:
    :param userid:
    :return:
    '''
    get_code('bingkolo','Win',userid,0,device)
    code = get_redis('code', userid)
    platregist('bingkolo','bingkolo-Win','Win',code,userid,'11111111',device)
    token = get_token('bingkolo','Win',userid,'11111111',device)
    try:
        loginreq = login_req(userid, token, 'bingkolo', env,Device=device)
        loginres = login_res()
        loginreqmsg = presssend(1001, random_fournumber(), loginreq)
        Head, body = verifycmdid(conn, loginreqmsg, loginres, 1002)
        if Head.RetCode == 7:
            # 发送创建角色协议
            createactorreq = createactor_req('我是%s'%userid[0-3])
            createactorreqmsg = presssend(1003, random_fournumber(), createactorreq)
            createactorres = createactor_res()
            # 收到创建角色协议汇回包
            Head, body = verifycmdid(conn, createactorreqmsg, createactorres, 1004)
            if Head.RetCode == 0:
                logger.info('创角协议响应retcode为：%s,Accid是：%s,userid是：%s' % (Head.RetCode,body.ActorData.AccId,userid))
                skip_guide(conn)
                send_gm(conn,'#additem 75 100000')
                send_gm(conn, '#addAllRole')
            else:
                logger.info('创角协议响应retcode为：%s,userid是：%s' % (Head.RetCode,userid))
        else:
            logger.info('登录协议响应retcode为：%s,userid是：%s' % (Head.RetCode, userid))
    except Exception as e:
        logger.info('请求都G了%s'%e)

def anpai_room(conn):
    arrangeroleinroomreq = arrangeroleinroom_req(1010,1,1)
    arrangeroleinroomres = arrangeroleinroom_res()
    arrangeroleinroomreqmsg = presssend(1172, random_fournumber(), arrangeroleinroomreq)
    Head, body = verifycmdid(conn, arrangeroleinroomreqmsg, arrangeroleinroomres, 1173)
    if Head.RetCode == 0:
        pass
    else:
        logger.error('协议响应retcode为：%s,userid是：%s' % (Head.RetCode))

def anpai_group(conn):
    shortcutgroupreq = shortcutgroup_req(0,{'roleid0':1030,'TalentId0':1,'roleid1':1040,'TalentId1':1,'roleid2':1046,'TalentId2':1,'roleid3':1049,'TalentId3':1})
    shortcutgroupres = shortcutgroup_res()
    shortcutgroupreqmsg = presssend(1078, random_fournumber(), shortcutgroupreq)
    Head, body = verifycmdid(conn, shortcutgroupreqmsg, shortcutgroupres, 1079)
    if Head.RetCode == 0:
        pass
    else:
        logger.error('协议响应retcode为：%s,userid是：%s' % (Head.RetCode))

# if __name__ == '__main__':
#     bingfa_num = 1
#     xunhuan_num = 1
#     waittime = 0.3
#     useridlist = []
#     socketlist =[]
#     for x in range(1):
#         socket = tcp_conn()
#         socketlist.append(socket)
#         # useridlist.append(random_number())
#
#
#     start_time = time.time()
#     result_multi = multi_thread(xunhuan_num,bingfa_num,waittime,loginandcreatacter_yace)
#     end_time = time.time()
#     logger.info('==============================本次压测执行结束=================================================')
#     logger.info('并发数量：%s 个'%bingfa_num)
#     logger.info('循环执行次数：%s 个'%xunhuan_num)
#     logger.info('等待时间：%s 秒'%waittime)
#     get_reqresult(result_multi)
#     logger.info('执行用时：%.4f 秒'%(end_time - start_time))
# socket = tcp_conn()
# userid ='13411111116'
# token = get_token('bingkolo', 'Win', userid, '11111111')
# reloginreq = relogin_req(userid, token, 'bingkolo', env,Device='')
# reloginres = relogin_res()
# reloginresmsg = presssend(1229, random_fournumber(), reloginreq)
# Head, loginbody = verifycmdid(socket, reloginresmsg, reloginres, 1230)
# print(Head)
# loginreq = login_req(userid, token, 'bingkolo', env,IsCertification = 1,IsVirtual =1)
# loginres = login_res()
# loginresmsg = presssend(1001, random_fournumber(), loginreq)
# Head, loginbody = verifycmdid(socket, loginresmsg, loginres, 1002)
# print(Head)