# _*_coding:utf-8 _*_
from Bingkolo_Protobuf_Api_Test.base.base_api import *
from Bingkolo_Protobuf_Api_Test.base.Protocol import cs_login_pb2
import requests
from collections import Counter


def login_req(phone,Token,PlatType,Env,Account,IsCertification = 1,Device = 'f87e82a3fe6dc63080604d2f5670123051f2aa05',Age = 42,BrithDay = '1980-01-01',IsVirtual = 0):
    '''
    登录请求     --1001
    :param Account: 玩家userid
    :param Token:传入userid对应的token
    :param PlatType:传入平台类型
    :param Env:传入环境类型
    :param IsCertification:是否实名认证 0 否 1 是
    :param Device:设备信息,默认为：f87e82a3fe6dc63080604d2f5670123051f2aa05
    :param Age:玩家年龄，默认为42
    :param BrithDay:玩家身份证上的生日，默认为'1980-01-01'
    :param IsVirtual:是否虚拟账号 0 否 1 是
    :param phone: 传入平台已注册的手机号码
    :return:
    '''
    cslogin = cs_login_pb2.CSLoginReq()
    cslogin.UserId = Account
    cslogin.Token = Token
    cslogin.PlatType = PlatType
    cslogin.ReLogin = 0
    cslogin.Age = Age
    cslogin.BrithDay = BrithDay
    cslogin.Pi = 'pi'
    cslogin.Di = '1'
    cslogin.Proj = 'xxx_master'
    cslogin.Version = "2.0.0"
    cslogin.SourceVersion = "2.0.0.0"
    cslogin.OS = 'Win'
    cslogin.Env = '3'
    cslogin.Revision = 52337
    cslogin.IsVirtual = IsVirtual #是否虚拟账号 0 否 1 是
    cslogin.IsCertification = IsCertification #是否实名认证 0 否 1 是
    cslogin.Device = Device #设备信息
    cslogin.Account = phone
    # cslogin.Index = Index  #账号序号，默认为0
    return cslogin

def login_res():
    '''
    -- 1002
    :return:
    '''
    CSLoginRes = cs_login_pb2.CSLoginRes()
    return CSLoginRes


def relogin_req(Account,Token,PlatType,Env,phone,Age = 42,BrithDay = '1980-01-01',Device = 'f87e82a3fe6dc63080604d2f5670123051f2aa05',Index = 0):
    '''
    重新登录请求  --1229
    :param Account:玩家userid
    :param Token:传入userid对应的token
    :param PlatType:传入平台类型
    :param Age:玩家年龄，默认为42
    :param BrithDay:玩家身份证上的生日，默认为'1980-01-01'
    :param Env:传入环境类型
    :param Device:设备信息,默认为：f87e82a3fe6dc63080604d2f5670123051f2aa05
    :param phone: 传入平台已注册的手机号码
    :return:
    '''
    csrelogin = cs_login_pb2.CSReLoginReq()
    csrelogin.UserId = Account
    csrelogin.Token = Token
    csrelogin.PlatType = PlatType
    csrelogin.Age = Age
    csrelogin.BrithDay = BrithDay
    csrelogin.Pi = 'pi'
    csrelogin.Di = '1'
    csrelogin.Version = "1.0.0"
    csrelogin.SourceVersion = "1.0.0.0"
    csrelogin.OS = 'Win'
    csrelogin.Env = Env
    csrelogin.Revision = 52337
    csrelogin.Device = Device #设备信息
    # csrelogin.Index = Index  #字段废弃
    csrelogin.Account = phone
    return csrelogin

def relogin_res():
    '''
    -- 1230
    :return:
    '''
    CSreloginRes = cs_login_pb2.CSReLoginRes()
    return CSreloginRes

def kickoff():
    '''
    踢人通知notify -- 1009
    :return:
    '''
    CSKickOffNotify = cs_login_pb2.CSKickOffNotify()
    return CSKickOffNotify

def createactor_req(name):
    '''
    创角协议  --1003
    :param name: 传入创建名称时的玩家名称
    :return:
    '''
    cscreateactorreq = cs_login_pb2.CSCreateActorReq()
    cscreateactorreq.Name = name
    return cscreateactorreq

def createactor_res():
    '''
    -- 1004
    :return:
    '''
    cscreateactorres = cs_login_pb2.CSCreateActorRes()
    return cscreateactorres


def playerdatanotify():
    '''
    玩家数据变动的消息通知  -- 1067
    :return:
    '''
    CSDataNotify = cs_login_pb2.CSDataNotify()
    return CSDataNotify

def redpointnotify():
    '''
    玩家红点推送 -- 1088
    :return:
    '''
    CSRedPointNotify = cs_login_pb2.CSRedPointNotify()
    return CSRedPointNotify


def dailyrefreshnotify():
    '''
    每日刷新通知 --1123
    :return:
    '''
    CSDailyRefreshNotify = cs_login_pb2.CSDailyRefreshNotify()
    return  CSDailyRefreshNotify



def actionreportlevelchangenotify():
    '''
    action上报等级变动通知 -- 1144
    action上报等级 默认0 数值越大，上报等级越高，对应上报action事件越多
    :return:
    '''
    CSActionReportLevelChangeNotify = cs_login_pb2.CSActionReportLevelChangeNotify()
    return CSActionReportLevelChangeNotify

def updateversionnotify():
    '''
    热更通知 --1223
    :return:
    '''
    CSUpdateVersionNotify = cs_login_pb2.CSUpdateVersionNotify()
    return CSUpdateVersionNotify

def forbidnotify():
    '''
    封号通知  --1225
    :return:
    '''
    CSForbidNotify = cs_login_pb2.CSForbidNotify()
    return CSForbidNotify


def logout_req():
    '''
    登出请求 --1341
    :return:
    '''
    CSLogoutReq = cs_login_pb2.CSLogoutReq()
    return CSLogoutReq

def logout_res():
    '''
    登出响应 --1342
    :return:
    '''
    CSLogoutRes = cs_login_pb2.CSLogoutRes()
    return CSLogoutRes

def getgamedata_req(Type):
    '''
    获取玩家游戏数据 --1357
    :param Type:想要获取的数据类型，为空则获取全部(EGDT_NONE = 0无效值;EGDT_MONEY = 1货币+体力数据;EGDT_STAGE_SCORE = 2章节数据;EGDT_STAGE_CHAPTER = 3关卡数据;EGDT_MODULE = 4模块数据;EGDT_ROLE = 5角色数据
    :return:
    '''
    CSGetGameDataReq = cs_login_pb2.CSGetGameDataReq()
    CSGetGameDataReq.Type = Type
    return CSGetGameDataReq

def getgamedata_res():
    '''
    获取玩家游戏数据响应 --1358
    :return:
    '''
    CSGetGameDataRes = cs_login_pb2.CSGetGameDataRes()
    return CSGetGameDataRes

'''
=============================================================================================================================================================
下面的方法是平台服的http方法
'''

def get_token(platType, os, UserId, password,device = 'f87e82a3fe6dc63080604d2f5670123051f2aa05'):
    '''
    平台服登录接口：/api/acc/v1/login，
    :return:平台登录token
    :param platType: 渠道
    :param os: 系统
    :param UserId: 玩家手机号码
    :param password: 账号对应密码
    :param device: 设备码，默认为：f87e82a3fe6dc63080604d2f5670123051f2aa05
    :return: 返回token和userid
    '''
    date = {
        "gameId": 1000,
        "platType": platType,
        "phone": UserId,
        "timestamp": now_timestamp(),
        "device": device,
        "os": os,
        "password": password,
        "sign": "771e206672ba5a78c13f5c0831546d56"
    }
    date = encryption(date)
    try:
        s = requests.session()
        req = s.post(accUri + '/api/acc/v1/login', json=date, verify=False)
        req_json = req.json()
        if req_json['code'] == 0:
            logger.debug('平台服登录成功%s'%req_json)
            return req_json['data']['token'],req_json['data']['userId']
        else:
            logger.error('平台登录接口返回状态码不为0，响应为：%s'%req_json['code'])
    except OSError as e:
        logger.error('平台登录接口：%s' % e)


def platlogin(UserId,password,device):
    '''
    GM平台登录接口：/api/base/login，需要填写gameid、渠道类型、操作系统、手机号、密码，返回token
    gameId：填写游戏ID，默认为1000
    platType：填写渠道类型
    os：填写iOS/Win/Android
    phone：填写需要注册的手机号
    password：填写密码
    :return:平台登录token
    '''
    date = {
        "gameId": "1000",
        "platType": "wx",
        "timestamp": now_timestamp(),
        "username": UserId,
        "password": password,
        "captcha": "111111",
        "deviceid": device,
        "sign": "f71721fb93f5cc2b3eb0cdf35b2674db"
    }
    date = encryption(date)
    try:
        s = requests.session()
        req = s.post(platuri + '/api/base/login', json=date, verify=False)
        req_json = req.json()
        if req_json['code'] == 0:
            logger.debug('token 是:%s'%req_json['data']['token'])
            return req_json['data']['token']
        else:
            logger.error('平台登录接口返回状态码不为0，响应为：%s'%req_json['code'])
    except OSError as e:
        logger.error('GM平台登录接口：%s' % e)

def updateplayer(token,accId):
    header = {'x-token':token}
    date = {"accId": [accId], "rangeType": 0, "items": "[]", "roles": "[]", "stages": "[]", "env": "product"}
    try:
        s = requests.session()
        req = s.post(platuri + '/api/gmapi/updateGmPlayer', headers=header,json=date, verify=False)
        req_json = req.json()
        if req_json['code'] == 0:
            logger.debug('返回信息是:%s'%req_json)
            # return req_json['data']
        else:
            logger.error('平台登录接口返回状态码不为0，响应为：%s'%req_json['code'])
    except OSError as e:
        logger.error('平台登录接口：%s' % e)

def platregist(platType,subplatType, os, code, phone, password,device):
    '''
    平台服注册接口：/api/acc/v1/regist，需要填写渠道类型、子渠道类型、操作系统、验证码、手机号
    platType：填写渠道类型
    subplatType：填写子渠道类型
    os：填写iOS/Win/Android
    code：短信验证码
    phone：填写需要注册的手机号
    device:设备信息
    '''
    date = {
        "gameId": 1000,
        "platType": platType,
        "phone": str(phone),
        "timestamp": now_timestamp(),
        "os": os,
        "subPlatType":subplatType,
        "device": device,
        "password": password,
        "code": int(code),
        "sign": "09f83f03637026c1fea3e247fe930054"
    }
    date = encryption(date)
    try:
        s = requests.session()
        req = s.post(accUri + '/api/acc/v1/regist', json=date, verify=False)
        req_json = req.json()
        if req_json['code'] == 0:
            logger.info('平台服创建账号成功')
            # return req_json['data']
        else:
            logger.error('平台创建账号接口返回状态码不为0，响应为：%s'%req_json['code'])
    except:
        logger.error('/api/acc/v1/regist接口报错，没有正常响应')

def get_code(platType, os, phone, codeType,device):
    '''
    /api/acc/v1/getCode接口，验证码可用于登录和注册
    platType：填写渠道类型
    os：填写iOS/Win/Android
    phone：填写获取验证码的手机号
    codeType：填写int类型的0(当前为注册账号页面)/1(当前为登录页面)
    device:设备信息
    '''
    date = {
        "gameId": 1000,
        "platType": platType,
        "phone": str(phone),
        "os": os,
        "device": device,
        "timestamp": now_timestamp(),
        "codeType": codeType,
        "sign": "84d47829eb1fb40849431e918568cdb3"
    }
    date = encryption(date)
    try:
        s = requests.session()
        req = s.post(accUri + '/api/acc/v1/getCode', json=date, verify=False)
        req_json = req.json()

        logger.info('/api/acc/v1/getCode接口正常响应，返回内容为%s' % req_json)
        return req_json
    except:
        logger.error('/api/acc/v1/getCode接口报错，没有正常响应')


