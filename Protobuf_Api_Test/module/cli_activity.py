# _*_coding:utf-8 _*_
from Bingkolo_Protobuf_Api_Test.module.cli_login import *
from Bingkolo_Protobuf_Api_Test.base.Protocol import cs_activity_pb2
from Bingkolo_Protobuf_Api_Test.module.cli_item import *

def getfourteensigninfo_req():
    '''
    获取14天签到面板请求 --1289
    :return:
    '''
    CSGetFourteenSignInfoReq = cs_activity_pb2.CSGetFourteenSignInfoReq()
    return CSGetFourteenSignInfoReq

def getfourteensigninfo_res():
    '''
    获取14天签到面板响应 --1290
    :return:
    '''
    CSGetFourteenSignInfoRes = cs_activity_pb2.CSGetFourteenSignInfoRes()
    return CSGetFourteenSignInfoRes

def getfourteensignInfonotify():
    '''
    14天签到面板信息通知 --1291
    :return:
    '''
    CSGetFourteenSignInfoNotify = cs_activity_pb2.CSGetFourteenSignInfoNotify()
    return CSGetFourteenSignInfoNotify

def signfourteensign_req(Day):
    '''
    14天签到--签到领取奖励请求  --1292
    :param Day: 传入需要签到的天数
    :return:
    '''
    CSSignFourteenSignReq = cs_activity_pb2.CSSignFourteenSignReq()
    CSSignFourteenSignReq.Day = Day
    return CSSignFourteenSignReq

def signfourteensign_res():
    '''
    --1293
    :return:
    '''
    CSSignFourteenSignRes = cs_activity_pb2.CSSignFourteenSignRes()
    return CSSignFourteenSignRes

def getactivitytimeinfo_req():
    '''
    获取活动时间信息请求 --1347
    :return:
    '''
    CSGetActivityTimeInfoReq = cs_activity_pb2.CSGetActivityTimeInfoReq()
    return CSGetActivityTimeInfoReq

def getactivitytimeinfo_res():
    '''
    --1348
    :return:
    '''
    CSGetActivityTimeInfoRes = cs_activity_pb2.CSGetActivityTimeInfoRes()
    return CSGetActivityTimeInfoRes

def activitychangenotify():
    '''
    活动变更通知  --1349
    :return:
    '''
    CSActivityChangeNotify = cs_activity_pb2.CSActivityChangeNotify()
    return CSActivityChangeNotify

def  rechargerebateinfo_req():
    '''
    充值返利面板获取请求 --1353
    :return:
    '''
    CSRechargeRebateInfoReq = cs_activity_pb2.CSRechargeRebateInfoReq()
    return CSRechargeRebateInfoReq

def  rechargerebateinfo_res():
    '''
    充值返利面板获取请求 --1354
    :return:
    '''
    CSRechargeRebateInfoRes = cs_activity_pb2.CSRechargeRebateInfoRes()
    return CSRechargeRebateInfoRes

def getrechargerebate_req():
    '''
    充值返利领取请求 -- 1355
    :return:
    '''
    CSGetRechargeRebateReq = cs_activity_pb2.CSGetRechargeRebateReq()
    return CSGetRechargeRebateReq

def getrechargerebate_res():
    '''
    充值返利领取响应 -- 1356
    :return:
    '''
    CSGetRechargeRebateRes = cs_activity_pb2.CSGetRechargeRebateRes()
    return CSGetRechargeRebateRes

