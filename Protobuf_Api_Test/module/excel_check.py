# _*_coding:utf-8 _*_
import operator
from Bingkolo_Protobuf_Api_Test.base.base_api import *


def get_rolejsitems(rolelist,level):
    '''
    获取舍友对应晋升阶段所需材料
    :param list: 传入舍友ID的list，参考[1001,1002,1003]
    :param level: 对应舍友晋升等级0,1,2
    :return:以字典格式返回对应舍友的晋升消耗{1001: {110: 2, 20: 2, 162: 2, 2: 20000}, 1002: {114: 2, 17: 2, 161: 2, 2: 20000}, 1003: {108: 2, 14: 2, 161: 2, 2: 20000}}
    '''
    js_rolejson = get_excelcontent('yc_养成配置表', 5, 6, 1)
    dict = {}
    if len(rolelist) != 0:
        for i in range(len(js_rolejson)):
            if js_rolejson[i]['干员ID'] in rolelist and js_rolejson[i]['晋升阶段'] == int(level):
                #创建嵌套字典
                dict.setdefault(js_rolejson[i]['干员ID'], {})[js_rolejson[i]['物品ID1']] = js_rolejson[i]['物品数量1']
                dict.setdefault(js_rolejson[i]['干员ID'], {})[js_rolejson[i]['物品ID2']] = js_rolejson[i]['物品数量2']
                dict.setdefault(js_rolejson[i]['干员ID'], {})[js_rolejson[i]['物品ID3']] = js_rolejson[i]['物品数量3']
                dict.setdefault(js_rolejson[i]['干员ID'], {})[js_rolejson[i]['物品ID4']] = js_rolejson[i]['物品数量4']

            else:
                pass
    else:
        logger.error('当前没有需要查询的舍友')

    logger.info('当前查询的舍友对应晋升等级%s的消耗如下：%s' % (level,dict))
    return dict

def get_daixunjsitems(rolelist,level):
    '''
    获取戴老板配表里预期想配置的晋升数据
    :param list: 传入舍友ID的list，参考[1001,1002,1003]
    :param level: 对应舍友晋升等级0,1,2
    :return:以字典格式返回对应舍友的晋升消耗{1001: {110: 2, 20: 2, 162: 2, 2: 20000}, 1002: {114: 2, 17: 2, 161: 2, 2: 20000}, 1003: {108: 2, 14: 2, 161: 2, 2: 20000}}
    '''
    excel_json = openWorkbook('F:\角色养成消耗.xlsx', 1, 2,0)
    dict = {}
    if len(rolelist) != 0:
        for i in range(len(excel_json)):
            if excel_json[i]['ID'] in rolelist and int(level) == 1:
                #创建嵌套字典
                dict.setdefault(excel_json[i]['ID'], {})[excel_json[i]['物品ID1']] = excel_json[i]['数量1']
                dict.setdefault(excel_json[i]['ID'], {})[excel_json[i]['物品ID2']] = excel_json[i]['数量2']
                dict.setdefault(excel_json[i]['ID'], {})[excel_json[i]['物品ID3']] = excel_json[i]['数量3']
                dict.setdefault(excel_json[i]['ID'], {})[excel_json[i]['物品ID4']] = excel_json[i]['数量4']
            elif excel_json[i]['ID'] in rolelist and int(level) == 2:
                dict.setdefault(excel_json[i]['ID'], {})[excel_json[i]['物品ID5']] = excel_json[i]['数量5']
                dict.setdefault(excel_json[i]['ID'], {})[excel_json[i]['物品ID6']] = excel_json[i]['数量6']
                dict.setdefault(excel_json[i]['ID'], {})[excel_json[i]['物品ID7']] = excel_json[i]['数量7']
                dict.setdefault(excel_json[i]['ID'], {})[excel_json[i]['物品ID8']] = excel_json[i]['数量8']
            else:
                pass
    else:
        logger.error('当前没有需要查询的舍友')

    logger.info('戴老板配表内对应晋升等级%s的消耗如下：%s' % (level,dict))
    return dict

def get_rolebynlitems(rolelist,level,rank):
    '''
    获取舍友对应本源能力等级所需材料
    :param list: 传入舍友ID的list，参考[1001,1002,1003]
    :param level: 对应舍友本源能力等级1,2,3
    :param rank: SSR舍友的2套本源能力组，分别对应1,2
    :return:以字典格式返回对应舍友的本源能力升级消耗{1001: {110: 2, 20: 2, 162: 2, 2: 20000}, 1002: {114: 2, 17: 2, 161: 2, 2: 20000}, 1003: {108: 2, 14: 2, 161: 2, 2: 20000}}
    '''
    bynl_rolejson = get_excelcontent('yc_养成配置表', 5, 6, 12)
    dict = {}
    if len(rolelist) != 0:
        for i in range(len(bynl_rolejson)):
            if bynl_rolejson[i]['干员ID'] in rolelist and bynl_rolejson[i]['等级'] == int(level) and bynl_rolejson[i]['本源组id']==rank:
                #创建嵌套字典
                dict.setdefault(bynl_rolejson[i]['干员ID'], {})[bynl_rolejson[i]['物品ID1']] = bynl_rolejson[i]['物品数量1']
                dict.setdefault(bynl_rolejson[i]['干员ID'], {})[bynl_rolejson[i]['物品ID2']] = bynl_rolejson[i]['物品数量2']
                dict.setdefault(bynl_rolejson[i]['干员ID'], {})[bynl_rolejson[i]['物品ID3']] = bynl_rolejson[i]['物品数量3']
                dict.setdefault(bynl_rolejson[i]['干员ID'], {})[bynl_rolejson[i]['物品ID4']] = bynl_rolejson[i]['物品数量4']

            else:
                pass
    else:
        logger.error('当前没有需要查询的舍友')

    logger.info('当前查询的舍友对应本源能力等级%s，本源组ID：%s的消耗如下：%s' % (level,rank,dict))
    return dict

def get_daixunbynlitems(rolelist,level,rank):
    '''
    获取戴老板配表里预期想配置的本源能力提升数据
    :param list: 传入舍友ID的list，参考[1001,1002,1003]
    :param level: 对应舍友晋升等级1,2,3
    :param rank: SSR舍友的2套本源能力组，分别对应1,2
    :return:以字典格式返回对应舍友的本源能力升级消耗{1001: {110: 2, 20: 2, 162: 2, 2: 20000}, 1002: {114: 2, 17: 2, 161: 2, 2: 20000}, 1003: {108: 2, 14: 2, 161: 2, 2: 20000}}
    '''
    excel_json = openWorkbook('F:\角色养成消耗.xlsx', 1, 2,1)
    dict = {}
    if len(rolelist) != 0:
        for i in range(len(excel_json)):
            if excel_json[i]['角色ID'] in rolelist and excel_json[i]['等级'] == int(level) and excel_json[i]['天赋组'] ==rank:
                #创建嵌套字典
                dict.setdefault(excel_json[i]['角色ID'], {})[excel_json[i]['道具ID1']] = excel_json[i]['数量1']
                dict.setdefault(excel_json[i]['角色ID'], {})[excel_json[i]['道具ID2']] = excel_json[i]['数量2']
                dict.setdefault(excel_json[i]['角色ID'], {})[excel_json[i]['道具ID3']] = excel_json[i]['数量3']
                dict.setdefault(excel_json[i]['角色ID'], {})[excel_json[i]['道具ID4']] = excel_json[i]['数量4']
            else:
                pass
    else:
        logger.error('当前没有需要查询的舍友')
    logger.info('戴老板配置的舍友对应本源能力等级%s，本源组ID：%s的消耗如下：%s' % (level,rank,dict))
    return dict


def get_rolezzsyitems(rolelist,level):
    '''
    获取舍友对应作战素养等级阶段所需材料
    :param list: 传入舍友ID的list，参考[1001,1002,1003]
    :param level: 对应舍友作战素养等级1,2,3
    :return:以字典格式返回对应舍友的作战素养升级消耗{1001: {110: 2, 20: 2, 162: 2, 2: 20000}, 1002: {114: 2, 17: 2, 161: 2, 2: 20000}, 1003: {108: 2, 14: 2, 161: 2, 2: 20000}}
    '''
    js_rolejson = get_excelcontent('yc_养成配置表', 5, 6, 13)
    dict = {}
    if len(rolelist) != 0:
        for i in range(len(js_rolejson)):
            if js_rolejson[i]['干员ID'] in rolelist and js_rolejson[i]['等级'] == int(level):
                #创建嵌套字典
                dict.setdefault(js_rolejson[i]['干员ID'], {})[js_rolejson[i]['物品ID1']] = js_rolejson[i]['物品数量1']
                dict.setdefault(js_rolejson[i]['干员ID'], {})[js_rolejson[i]['物品ID2']] = js_rolejson[i]['物品数量2']
                dict.setdefault(js_rolejson[i]['干员ID'], {})[js_rolejson[i]['物品ID3']] = js_rolejson[i]['物品数量3']
                dict.setdefault(js_rolejson[i]['干员ID'], {})[js_rolejson[i]['物品ID4']] = js_rolejson[i]['物品数量4']

            else:
                pass
    else:
        logger.error('当前没有需要查询的舍友')

    logger.info('当前查询的舍友对应作战素养等级%s的消耗如下：%s' % (level,dict))
    return dict

def get_daixunzzsyitems(rolelist,level):
    '''
    获取戴老板配表里预期想配置的作战素养数据
    :param list: 传入舍友ID的list，参考[1001,1002,1003]
    :param level: 对应舍友晋升等级1,2,3
    :return:以字典格式返回对应舍友的作战素养升级消耗{1001: {110: 2, 20: 2, 162: 2, 2: 20000}, 1002: {114: 2, 17: 2, 161: 2, 2: 20000}, 1003: {108: 2, 14: 2, 161: 2, 2: 20000}}
    '''
    excel_json = openWorkbook('F:\角色养成消耗.xlsx', 1, 2,2)
    dict = {}
    if len(rolelist) != 0:
        for i in range(len(excel_json)):
            if excel_json[i]['角色ID'] in rolelist and excel_json[i]['等级'] == int(level):
                #创建嵌套字典
                dict.setdefault(excel_json[i]['角色ID'], {})[excel_json[i]['道具1ID']] = excel_json[i]['数量1']
                dict.setdefault(excel_json[i]['角色ID'], {})[excel_json[i]['道具2ID']] = excel_json[i]['数量2']
                dict.setdefault(excel_json[i]['角色ID'], {})[excel_json[i]['道具3ID']] = excel_json[i]['数量3']
                dict.setdefault(excel_json[i]['角色ID'], {})[excel_json[i]['道具4ID']] = excel_json[i]['数量4']
            else:
                pass
    else:
        logger.error('当前没有需要查询的舍友')

    logger.info('戴老板配表内对应作战素养等级%s的消耗如下：%s' % (level,dict))
    return dict

# def getminorid():
#     '''
#     获取配表内的
#     :return:
#     '''
#     lookitem_dict = {}
#     minoridlist = []
#     stage_minor_json = get_excelcontent('gq_关卡信息表', 5, 6, 2)
#
#     for i in range(len(stage_minor_json)):
#         if stage_minor_json[i]['关卡id:key'] < 40000:
#             lookitem_dict['关卡ID%s'%i] = stage_minor_json[i]['关卡id:key']
#         else:
#             pass
#     minoridlist = list(lookitem_dict.values())
#     return minoridlist


def getlookdropitem():
    '''
    获取关卡配置表里配置的展示的掉落信息和关卡IDlist
    :return:lookitem_dict：关卡配置表里配置的展示的掉落信息
    :return:minoridlist:关卡IDlist
    '''
    lookitem_dict = {}
    minorid_dict = {}
    stage_minor_json = get_excelcontent('gq_关卡信息表', 5, 6, 1)
    minoridlist = []
    for i in range(len(stage_minor_json)):
        if stage_minor_json[i]['关卡id:key'] not in (40101,40102,40103,40104):
            minorid_dict['关卡ID%s' % i] = stage_minor_json[i]['关卡id:key']
            minoridlist = list(minorid_dict.values())
            for x in range(stage_minor_json[i]['总道具数量(测试用)']):
                x += 1
                if stage_minor_json[i]['产出方式%sn1: 首通三星n2：首通获得n3: 固定获得n4: 概率获得' % x] == 1:
                    stage_minor_json[i]['产出方式%sn1: 首通三星n2：首通获得n3: 固定获得n4: 概率获得' % x] = '首通三星'
                elif stage_minor_json[i]['产出方式%sn1: 首通三星n2：首通获得n3: 固定获得n4: 概率获得' % x] == 2:
                    stage_minor_json[i]['产出方式%sn1: 首通三星n2：首通获得n3: 固定获得n4: 概率获得' % x] = '首通获得'
                elif stage_minor_json[i]['产出方式%sn1: 首通三星n2：首通获得n3: 固定获得n4: 概率获得' % x] == 3:
                    stage_minor_json[i]['产出方式%sn1: 首通三星n2：首通获得n3: 固定获得n4: 概率获得' % x] = '固定获得'
                elif stage_minor_json[i]['产出方式%sn1: 首通三星n2：首通获得n3: 固定获得n4: 概率获得' % x] == 4:
                    stage_minor_json[i]['产出方式%sn1: 首通三星n2：首通获得n3: 固定获得n4: 概率获得' % x] = '概率获得'
                if stage_minor_json[i]['产出概率%sn1:极低概率n2:低概率n3:中等概率n4:较高概率n5:固定产出'%x] == 1:
                    stage_minor_json[i]['产出概率%sn1:极低概率n2:低概率n3:中等概率n4:较高概率n5:固定产出' % x] = '极低概率'
                elif stage_minor_json[i]['产出概率%sn1:极低概率n2:低概率n3:中等概率n4:较高概率n5:固定产出'%x] == 2:
                    stage_minor_json[i]['产出概率%sn1:极低概率n2:低概率n3:中等概率n4:较高概率n5:固定产出'%x] = '低概率'
                elif stage_minor_json[i]['产出概率%sn1:极低概率n2:低概率n3:中等概率n4:较高概率n5:固定产出'%x] == 3:
                    stage_minor_json[i]['产出概率%sn1:极低概率n2:低概率n3:中等概率n4:较高概率n5:固定产出'%x] = '中等概率'
                elif stage_minor_json[i]['产出概率%sn1:极低概率n2:低概率n3:中等概率n4:较高概率n5:固定产出'%x] == 4:
                    stage_minor_json[i]['产出概率%sn1:极低概率n2:低概率n3:中等概率n4:较高概率n5:固定产出'%x] = '较高概率'
                elif stage_minor_json[i]['产出概率%sn1:极低概率n2:低概率n3:中等概率n4:较高概率n5:固定产出'%x] == 5:
                    stage_minor_json[i]['产出概率%sn1:极低概率n2:低概率n3:中等概率n4:较高概率n5:固定产出'%x] = '固定产出'
                if stage_minor_json[i]['产出方式%sn1: 首通三星n2：首通获得n3: 固定获得n4: 概率获得'%x] == '':
                    pass
                else:
                    if stage_minor_json[i]['物品数量%s'%x] == '':
                        stage_minor_json[i]['物品数量%s' % x] = '空'
                    else:
                        pass
                    # lookitem_dict.setdefault(stage_minor_json[i]['关卡id:key'], {})['产出方式'] = stage_minor_json[i]['产出方式%sn1: 首通三星n2：首通获得n3: 固定获得n4: 概率获得' % x]
                    # lookitem_dict.setdefault(stage_minor_json[i]['关卡id:key'], {})['产出概率'] = stage_minor_json[i]['产出概率%sn1:极低概率n2:低概率n3:中等概率n4:较高概率n5:固定产出'%x]
                    lookitem_dict.setdefault(stage_minor_json[i]['关卡id:key'], {})[stage_minor_json[i]['物品Id%s'%x]] = stage_minor_json[i]['物品数量%s'%x]
        else:
            pass
    # logger.info('当前配置的关卡展示的物品信息dict如下：%s'%lookitem_dict)
    # logger.info('当前配置的关卡ID如下：%s'%minoridlist)
    return lookitem_dict,minoridlist

def getminordrop():
    '''
    获取关卡配置表里配置的实际的掉落信息
    :return1:stageminordrop_dict：关卡表stage_minor内的3星掉落的掉落组ID
    :return2:stagedrop_dict：关卡表stage_drop内的掉落组数据
    '''
    stageminordrop_dict = {}
    stagedrop_dict = {}
    stage_minor_json = get_excelcontent('gq_关卡信息表', 5, 6, 1)
    stage_drop_json = get_excelcontent('gq_关卡信息表', 5, 6, 3)
    for i in range(len(stage_minor_json)):
        # list = stage_minor_json[i]['掉落组 Idn未通关;1星;2星;3星n无尽模式为对应层数奖励'].split(';')[3]
        stageminordrop_dict[stage_minor_json[i]['关卡id:key']] = int(stage_minor_json[i]['掉落组 Idn未通关;1星;2星;3星n无尽模式为对应层数奖励'].split(';')[3])
    # print(stageminordrop_dict)
    for x in range(len(stage_drop_json)):
        for y in range(5):
            y += 1
            if stage_drop_json[x]['物品Id%s' % y] == '':
                pass
            else:
            #     if stage_drop_json[x]['掉落类型%sn1: 首次掉落n2: 常规掉落n3: 额外物资 4：首次三星  5：活动掉落' % y] == 1:
            #         stage_drop_json[x]['掉落类型%sn1: 首次掉落n2: 常规掉落n3: 额外物资 4：首次三星  5：活动掉落' % y] = '首次掉落'
            #     elif stage_drop_json[x]['掉落类型%sn1: 首次掉落n2: 常规掉落n3: 额外物资 4：首次三星  5：活动掉落' % y] == 2:
            #         stage_drop_json[x]['掉落类型%sn1: 首次掉落n2: 常规掉落n3: 额外物资 4：首次三星  5：活动掉落' % y] = '常规掉落'
            #     elif stage_drop_json[x]['掉落类型%sn1: 首次掉落n2: 常规掉落n3: 额外物资 4：首次三星  5：活动掉落' % y] == 3:
            #         stage_drop_json[x]['掉落类型%sn1: 首次掉落n2: 常规掉落n3: 额外物资 4：首次三星  5：活动掉落' % y] = '额外物资'
            #     elif stage_drop_json[x]['掉落类型%sn1: 首次掉落n2: 常规掉落n3: 额外物资 4：首次三星  5：活动掉落' % y] == 4:
            #         stage_drop_json[x]['掉落类型%sn1: 首次掉落n2: 常规掉落n3: 额外物资 4：首次三星  5：活动掉落' % y] = '首次三星'
            #     elif stage_drop_json[x]['掉落类型%sn1: 首次掉落n2: 常规掉落n3: 额外物资 4：首次三星  5：活动掉落' % y] == 5:
            #         stage_drop_json[x]['掉落类型%sn1: 首次掉落n2: 常规掉落n3: 额外物资 4：首次三星  5：活动掉落' % y] = '活动掉落'
            #     else:
            #         pass
                # stagedrop_dict.setdefault(stage_drop_json[x]['掉落组 Id'], {})['掉落类型%s'% y] = stage_drop_json[x]['掉落类型%sn1: 首次掉落n2: 常规掉落n3: 额外物资 4：首次三星  5：活动掉落' % y]
                stagedrop_dict.setdefault(stage_drop_json[x]['掉落组 Id'], {})['是否使用通用物品掉落%s'%y] = stage_drop_json[x]['%s是否使用通用物品掉落，使用时item_id填掉落ID，item_count填调用该掉落次数，权重值无效且不计入本次的权重掉落n0（或不填）：否n1：是' % y]
                stagedrop_dict.setdefault(stage_drop_json[x]['掉落组 Id'], {})['物品Id%s' % y] = stage_drop_json[x]['物品Id%s' % y]
                stagedrop_dict.setdefault(stage_drop_json[x]['掉落组 Id'], {})['物品数量%s' % y] = stage_drop_json[x]['物品数量%s' % y]
                # stagedrop_dict.setdefault(stage_drop_json[x]['掉落组 Id'], {})['权重%s' % y] = stage_drop_json[x]['权重%sn(0: 必定掉落)' % y]
                # stagedrop_dict.setdefault(stage_drop_json[x]['掉落组 Id'], {})['通关分数%s' % y] = stage_drop_json[x]['%s通关分数n0: 表示不限制n非0: 要达到之后才能获得,只有首次掉落才有意义' % y]
    return stageminordrop_dict,stagedrop_dict

def getitemdrop():
    '''
    查询掉落表的掉落配置
    :return:
    '''
    itemdrop_dict = {}
    itemdrop_json = get_excelcontent('dl_掉落表', 5, 6, 0)
    for i in range(len(itemdrop_json)):
        for y in range(10):
            y += 1
            if itemdrop_json[i]['物品%sID' % y] == '':
                pass
            else:
                itemdrop_dict.setdefault(itemdrop_json[i]['掉落ID'], {})['掉落类型'] = itemdrop_json[i]['掉落类型']
                itemdrop_dict.setdefault(itemdrop_json[i]['掉落ID'], {})['固有属性'] = itemdrop_json[i]['固有属性']
                itemdrop_dict.setdefault(itemdrop_json[i]['掉落ID'], {})['物品%sID'%y] = itemdrop_json[i]['物品%sID'%y]
                itemdrop_dict.setdefault(itemdrop_json[i]['掉落ID'], {})['物品%s数量'%y] = itemdrop_json[i]['物品%s数量'%y]
                itemdrop_dict.setdefault(itemdrop_json[i]['掉落ID'], {})['物品%s权重/概率万分比'%y] = itemdrop_json[i]['物品%s权重/概率万分比'%y]

    return itemdrop_dict
def getfinaldrop():
    '''
    返回从关卡掉落表和通用掉落表内组装好的掉落数据
    '''
    #获取关卡表内配置的展示掉落配置和关卡ID列表
    lookitemdict,minoridlist = getlookdropitem()
    #获取关卡表内配置的实际掉落配置和关卡掉落表配置
    stageminordrop_dict,stagedrop_dict = getminordrop()
    #获取通用掉落表内的配置
    itemdrop_dict = getitemdrop()

    #最终组装好的掉落信息
    finaldrop_dict = {}

    for i in minoridlist:

        dropid = int(len(stagedrop_dict[stageminordrop_dict[i]]) / 3)
        # for y in range(dropid):
        for x in range(dropid):
            x+=1
            finaldrop_dict.setdefault(i, {})[stagedrop_dict[stageminordrop_dict[i]]['物品Id%s' % x]] = stagedrop_dict[stageminordrop_dict[i]]['物品数量%s' % x]
            if stagedrop_dict[stageminordrop_dict[i]]['是否使用通用物品掉落%s'%x] == 1:
                del finaldrop_dict[i][stagedrop_dict[stageminordrop_dict[i]]['物品Id%s'%x]]
                if stagedrop_dict[stageminordrop_dict[i]]['物品Id%s'%x] in itemdrop_dict:
                    for dropx in range(itemdrop_dict[stagedrop_dict[stageminordrop_dict[i]]['物品Id%s'%x]]['固有属性']):
                        dropx += 1
                        finaldrop_dict.setdefault(i, {})[itemdrop_dict[stagedrop_dict[stageminordrop_dict[i]]['物品Id%s'%x]]['物品%sID'%dropx]] = itemdrop_dict[stagedrop_dict[stageminordrop_dict[i]]['物品Id%s' % x]]['物品%s数量' % dropx]
                        finaldrop_dict.setdefault(i, {})[2] = '空'
                    # print(finaldrop_dict.setdefault(i, {})['物品Id%s' % x])
                else:
                    logger.debug('没有找到掉落表的ID:%s'%stagedrop_dict[stageminordrop_dict[i]]['物品Id%s'%x])
                    pass

            else:
                pass
    return finaldrop_dict,lookitemdict

