# _*_coding:utf-8 _*_
import os,sys,pytest
curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(curPath)[0]
sys.path.append(rootPath)
from Bingkolo_Protobuf_Api_Test.base.base_api import *
from Bingkolo_Protobuf_Api_Test.module.cli_login import *



logincmd_dict = {'loginreq': 1001, 'loginres': 1002, 'createactorreq': 1003, 'createactorres': 1004,'reloginreq': 1229,'reloginres': 1230,'kickoffnotify': 1009}

status_list = get_statuslist()

rolelogin_user = '13422221111'
recruitlogin_user = '13422221111015'


# @pytest.mark.skip('1111111111111111111')
class Test_suite_login():
    '''
    登录测试用例
    '''

    def test_loginerror_code153(self):
        u'测试登录返回状态码153(Token失效LOGIN_ACCESS_TOKEN_INVAILD = 153)'
        loginreq = login_req('13422221111','111111111111111', 'bingkolo', env,'13422221111')
        loginreqmsg = presssend(logincmd_dict.get('loginreq'), 1, loginreq)
        sockconnerr = tcp_conn()
        loginres = login_res()
        Head, body = verifycmdid(sockconnerr, loginreqmsg, loginres, logincmd_dict.get('loginres'))
        logger.info('当前协议响应retcode为：%s'%Head.RetCode)
        tcpconn_close()
        assert Head.RetCode == 153
        assert Head.RetCode in status_list


    def test_loginerror_code5(self):
        u'测试登录返回状态码5(重复创角LOGIN_EXIST_ROLE = 5)'
        #登录游戏服已有数据的账号
        token,account = get_token('bingkolo','Win','13422221111','11111111')
        loginreq = login_req('13422221111',token, 'bingkolo', env,account)
        loginreqmsg = presssend(logincmd_dict.get('loginreq'), 1, loginreq)
        sockconnerr = tcp_conn()
        recvmsg = sendmsg(sockconnerr, loginreqmsg)
        loginres = login_res()
        pressrecvsend(recvmsg, loginres)
        #发送创建角色协议
        createactorreq = createactor_req('我是玩家名称')
        createactorreqmsg = presssend(logincmd_dict.get('createactorreq'), 3, createactorreq)
        createactorres = createactor_res()
        #收到创建角色协议汇回包
        Head, body = verifycmdid(sockconnerr, createactorreqmsg, createactorres, logincmd_dict.get('createactorres'))
        logger.info('当前协议响应retcode为：%s'%Head.RetCode)
        tcpconn_close()
        assert Head.RetCode == 5
        assert Head.RetCode in status_list

    def test_loginerror_code137(self):
        u'测试创建角色时名称非法(敏感词检测: 包含敏感词 SENSITIVE_HAVE_ONE = 137)'
        userid = random_number()
        token,account = get_token('bingkolo','Win',userid,'11111111')
        loginreq = login_req(userid,token, 'bingkolo', env,account)
        loginreqmsg = presssend(logincmd_dict.get('loginreq'), 1, loginreq)
        sockconnerr = tcp_conn()
        recvmsg = sendmsg(sockconnerr, loginreqmsg)
        loginres = login_res()
        pressrecvsend(recvmsg, loginres)
        #发送创建角色协议
        createactorreq = createactor_req('测试')
        createactorreqmsg = presssend(logincmd_dict.get('createactorreq'), 3, createactorreq)
        createactorres = createactor_res()
        #收到创建角色协议汇回包
        Head, body = verifycmdid(sockconnerr, createactorreqmsg, createactorres, logincmd_dict.get('createactorres'))
        logger.info('当前协议响应retcode为：%s'%Head.RetCode)
        tcpconn_close()
        assert Head.RetCode == 137
        assert Head.RetCode in status_list

    def test_loginerror_code7(self):
        u'测试登录返回状态码7(玩家还没有角色，需要创角LOGIN_CREATE_ROLE = 7)'
        userid = '56078321860'
        sockconnerr = tcp_conn()
        token,account = get_token('bingkolo','Win',userid,'11111111')
        loginreq = login_req(userid,token, 'bingkolo', env,account)
        loginreqmsg = presssend(logincmd_dict.get('loginreq'), 1, loginreq)
        loginres = login_res()
        #使用A连接发送登录请求
        Head, body = verifycmdid(sockconnerr, loginreqmsg, loginres, logincmd_dict.get('loginres'))
        logger.info('当前协议响应retcode为：%s' % Head.RetCode)
        assert Head.RetCode == 7
        assert Head.RetCode in status_list

    def test_loginerror_code8(self):
        u'防沉迷玩家在非可玩时间登录(防沉迷限制 LOGIN_ANTI_ADDICT = 8)'
        userid = '13411111116'
        sockconnerr = tcp_conn()
        token,account = get_token('bingkolo','Win',userid,'11111111')
        loginreq = login_req(userid,token, 'bingkolo', env,account,Age=11)
        loginreqmsg = presssend(logincmd_dict.get('loginreq'), 1, loginreq)
        loginres = login_res()
        #使用A连接发送登录请求
        Head, body = verifycmdid(sockconnerr, loginreqmsg, loginres, logincmd_dict.get('loginres'))
        logger.info('当前协议响应retcode为：%s' % Head.RetCode)
        assert Head.RetCode == 8
        assert Head.RetCode in status_list

    def test_loginerror_code20(self):
        u'测试登录返回状态码20(重复登录被踢下线KICKOFF_DUPLICATE = 20)'
        #登录游戏服已有数据的账号
        token,account = get_token('bingkolo','Win','13422221111011','11111111')
        loginreq = login_req('13422221111011',token, 'bingkolo', env,account)
        loginreqmsg = presssend(logincmd_dict.get('loginreq'), 1, loginreq)
        #使用A连接发送登录请求
        sockconnerr = tcp_conn()
        sendmsg(sockconnerr, loginreqmsg)
        #重复登录
        token1,account1 = get_token('bingkolo','Win','13422221111011','11111111')
        loginreq1 = login_req('13422221111011',token1, 'bingkolo', env,account1)
        loginreqmsg1 = presssend(logincmd_dict.get('loginreq'), 2, loginreq1)
        #使用B连接发送登录请求
        sockconnerr1 = tcp_conn()
        sendmsg(sockconnerr1, loginreqmsg1)
        #接收notify消息
        kickoffnotify = kickoff()
        notifymsg = notifyrecvmsg(sockconnerr)
        Head,body = pressrecvsend(notifymsg, kickoffnotify)
        logger.info('当前协议响应retcode为：%s'%Head.RetCode)
        tcpconn_close()
        assert Head.RetCode == 20
        assert Head.RetCode in status_list


    def test_loginerror_code182(self):
        u'测试登录返回状态码182(网络心跳超时，需要返回重登NEED_BACK_TO_LOGIN = 182)'
        #登录游戏服已有数据的账号
        token,account = get_token('bingkolo','Win','13422221111','11111111')
        reloginreq = relogin_req('134222211111',token, 'bingkolo', env,account)
        reloginreqmsg = presssend(logincmd_dict.get('reloginreq'), 1, reloginreq)
        sockconnerr = tcp_conn()
        reloginres = relogin_res()
        Head, body = verifycmdid(sockconnerr, reloginreqmsg, reloginres, logincmd_dict.get('reloginres'))
        logger.info('当前协议响应retcode为：%s'%Head.RetCode)
        tcpconn_close()
        assert Head.RetCode == 182
        assert Head.RetCode in status_list

    @pytest.mark.skip('需求调整，用例废除')
    def test_loginerror_code280(self):
        u'登录时传的玩家序号错误(玩家序号传入不正确Error_Player_Index = 280)'
        userid = '13411111116'
        sockconnerr = tcp_conn()
        token,account = get_token('bingkolo','Win',userid,'11111111')
        loginreq = login_req(userid,token, 'bingkolo', env,account)
        loginreqmsg = presssend(logincmd_dict.get('loginreq'), 1, loginreq)
        loginres = login_res()
        #使用A连接发送登录请求
        Head, body = verifycmdid(sockconnerr, loginreqmsg, loginres, logincmd_dict.get('loginres'))
        logger.info('当前协议响应retcode为：%s' % Head.RetCode)
        assert Head.RetCode == 280
        assert Head.RetCode in status_list

