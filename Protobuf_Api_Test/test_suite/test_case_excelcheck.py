# _*_coding:utf-8 _*_
import os,sys,pytest
curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(curPath)[0]
sys.path.append(rootPath)
from Bingkolo_Protobuf_Api_Test.base.base_api import *
from Bingkolo_Protobuf_Api_Test.module.cli_login import *
from Bingkolo_Protobuf_Api_Test.module.cli_role import *
from Bingkolo_Protobuf_Api_Test.module.cli_item import *
from Bingkolo_Protobuf_Api_Test.module.cli_chat import *
from Bingkolo_Protobuf_Api_Test.module.cli_recruit import *
from Bingkolo_Protobuf_Api_Test.module.cli_group import *
from Bingkolo_Protobuf_Api_Test.module.cli_task import *

# list2 =




class Test_suite_configurationcheck():
    '''
    验证养成配置数据预期的和实际配置到养成表里的是否一致
    '''
    @pytest.mark.skip('1111111')
    @pytest.mark.parametrize('roleid',[1001, 1002, 1003, 1004, 1005, 1006, 1008, 1009, 1010, 1011, 1012, 1015, 1016, 1017, 1018, 1019, 1020, 1021,
         1023, 1024, 1025, 1026,
         1027, 1028, 1029, 1030, 1031, 1032, 1033, 1034, 1035, 1036, 1037, 1038, 1039, 1040, 1041, 1042, 1043, 1044,
         1045, 1046, 1047, 1048,
         1049, 1050, 1051, 1052, 1053, 1054])
    def test_jstext_check(self,roleid):
        '''
        验证策划配置的晋升配置是否和他自己预期的一致
        '''
        dict_js1 = get_rolejsitems([roleid], 1)
        dict_js2 = get_rolejsitems([roleid], 2)
        dailaoban_js1 = get_daixunjsitems([roleid], 1)
        dailaoban_js2 = get_daixunjsitems([roleid], 2)
        assert dict_js1==dailaoban_js1
        assert dict_js2==dailaoban_js2

    @pytest.mark.skip('1111111')
    @pytest.mark.parametrize('roleid',[1001, 1002, 1003, 1004, 1005, 1006, 1008, 1009, 1010, 1011, 1012, 1015, 1016, 1017, 1018, 1019, 1020, 1021,
         1023, 1024, 1025, 1026,
         1027, 1028, 1029, 1030, 1031, 1032, 1033, 1034, 1035, 1036, 1037, 1038, 1039, 1040, 1041, 1042, 1043, 1044,
         1045, 1046, 1047, 1048,
         1049, 1050, 1051, 1052, 1053, 1054])
    def test_bynltext_check(self,roleid):
        '''
        验证策划配置的本源能力配置是否和他自己预期的一致
        '''
        rank1_bynl1 = get_rolebynlitems([roleid],1,1)
        daixun_rank1_bynl1 = get_daixunbynlitems([roleid],1,1)
        rank1_bynl2 = get_rolebynlitems([roleid],2,1)
        daixun_rank1_bynl2 = get_daixunbynlitems([roleid],2,1)
        rank1_bynl3 = get_rolebynlitems([roleid],3,1)
        daixun_rank1_bynl3 = get_daixunbynlitems([roleid],3,1)
        rank2_bynl1 = get_rolebynlitems([roleid],1,2)
        daixun_rank2_bynl1 = get_daixunbynlitems([roleid],1,2)
        rank2_bynl2 = get_rolebynlitems([roleid],2,2)
        daixun_rank2_bynl2 = get_daixunbynlitems([roleid],2,2)
        rank2_bynl3 = get_rolebynlitems([roleid],3,2)
        daixun_rank2_bynl3 = get_daixunbynlitems([roleid],3,2)
        assert rank1_bynl1 == daixun_rank1_bynl1
        assert rank1_bynl2 == daixun_rank1_bynl2
        assert rank1_bynl3 == daixun_rank1_bynl3
        assert rank2_bynl1 == daixun_rank2_bynl1
        assert rank2_bynl2 == daixun_rank2_bynl2
        assert rank2_bynl3 == daixun_rank2_bynl3


    @pytest.mark.skip('1111111')
    @pytest.mark.parametrize('roleid',[1001, 1002, 1003, 1004, 1005, 1006, 1008, 1009, 1010, 1011, 1012, 1015, 1016, 1017, 1018, 1019, 1020, 1021,
         1023, 1024, 1025, 1026,
         1027, 1028, 1029, 1030, 1031, 1032, 1033, 1034, 1035, 1036, 1037, 1038, 1039, 1040, 1041, 1042, 1043, 1044,
         1045, 1046, 1047, 1048,
         1049, 1050, 1051, 1052, 1053, 1054])
    def test_zzsytext_check(self,roleid):
        '''
        验证策划配置的作战素养配置是否和他自己预期的一致
        '''
        dict_zzsy1 = get_rolezzsyitems([roleid], 1)
        dict_zzsy2 = get_rolezzsyitems([roleid], 2)
        dict_zzsy3 = get_rolezzsyitems([roleid], 3)
        dailaoban_zzsy1 = get_daixunzzsyitems([roleid], 1)
        dailaoban_zzsy2 = get_daixunzzsyitems([roleid], 2)
        dailaoban_zzsy3 = get_daixunzzsyitems([roleid], 3)
        assert dict_zzsy1 == dailaoban_zzsy1
        assert dict_zzsy2 == dailaoban_zzsy2
        assert dict_zzsy3 == dailaoban_zzsy3

    @pytest.mark.parametrize('minorid',[31011, 31021, 31081, 31031, 1021, 31041, 1031, 31051, 1041, 1051, 31061, 1061, 1071, 1081, 31071, 1091, 1101, 1111,
     1121, 1131, 1141, 1151, 1161, 2011, 2012, 2021, 2022, 2031, 2032, 2041, 2042, 2051, 2052, 2061, 2062, 2071, 2072,
     2081, 2082, 2091, 2092, 2101, 2102, 2111, 2112, 2121, 2122, 2131, 2132, 2141, 2142, 2151, 2152, 2161, 2162, 2171,
     2172, 2182, 3011, 3012, 3021, 3022, 3031, 3032, 3041, 3042, 3051, 3052, 3061, 3062, 3071, 3072, 3081, 3082, 3091,
     3092, 3101, 3102, 3111, 3112, 3121, 3122, 3131, 3132, 3141, 3142, 3151, 3152, 3161, 3162, 3171, 3172, 3181, 3182,
     3191, 4011, 4012, 4021, 4022, 4031, 4032, 4041, 4042, 4051, 4052, 4061, 4062, 4071, 4072, 4081, 4082, 4091, 4092,
     4101, 4102, 4111, 4112, 4121, 4122, 4131, 4132, 4141, 4142, 4151, 4152, 4161, 4162, 4171, 4172, 4181, 4182, 4191,
     4192, 4201, 4202, 4221, 4211,105201,5011,5012,5021,5022,5031,5032,5041,5042,5051,5052,5061,5062,5071,5072,5081,5082,
     5091,5092,5101,5102,5111,5112,5121,5122,5131,5132,5141,5142,5151,5152,5161,5171,105211, 21011, 21021, 21031, 21041, 21051, 22011, 22021, 22031, 22041, 22051, 23011, 23021,
     23031, 23041, 23051,101151,51011,51012,51021,51022,51031,51032,51041,51042,51051,51052,51061,51062,51071,51072,51081,51082,51091,51092,51101,51102,101094,51111,
    101152,52011,52021,52031,52041,52051,52061,52081,52101,52111,52121,101153,52012,52022,52032,52042,52052,52062,52082,52102,52112,52122,52131])
    def test_drop_check(self,minorid):
        u'验证掉落表配置'
        # logger.info('当前关卡ID是%s'%minorid)
        final,lookdrop = getfinaldrop()

        finaldrop_dicts = (sorted(final[minorid].items(), key=operator.itemgetter(0)))
        lookitemdicts = (sorted(lookdrop[minorid].items(), key=operator.itemgetter(0)))
        text = u'关卡ID：%s的实际掉落配置和展示的不一致,展示的配置为：%s，实际掉落配置为：%s' % (
        minorid, lookitemdicts, finaldrop_dicts)
        if finaldrop_dicts != lookitemdicts:
            logger.info(text)
        else:
            pass

        assert finaldrop_dicts == lookitemdicts,text
