# _*_coding:utf-8 _*_

from Bingkolo_Protobuf_Api_Test.base.loggingSetting import *
from Bingkolo_Protobuf_Api_Test.base.Protocol import cs_head_pb2
from Bingkolo_Protobuf_Api_Test.base.Protocol import cs_common_pb2
from Bingkolo_Protobuf_Api_Test.base.Protocol import cs_enum_pb2
from Bingkolo_Protobuf_Api_Test.base.config import *
from Bingkolo_Protobuf_Api_Test.base.Protocol import cs_cmdvalue_pb2
from redis import Redis
# from data.Data import parseMsg
from socket import *
from struct import *
import os, time, threading, hashlib,random,xlrd,json

logger = log_set()


def update_proto():
    '''
    批量更新项目工程内的proto文件，非必要不更新，更新会导致proto.py内的引用出现问题
    :return:
    '''
    proto_list = []
    protopath = 'G:\kleins_client\BuildProtocol\Protocol'
    for file_name in os.listdir(protopath):
        if file_name[-6:-1] != '.prot':
            pass
        else:
            proto_list.append(file_name)
    for i in proto_list:
        command = '@echo y|protoc -I ' + protopath + ' --python_out=./Protocol/ ' + i
        logger.info('更新协议......%s' % command)
        os.system(command)

def get_md5(src):
    '''
    传入字符串，转化为对应的md5值
    '''
    myMd5 = hashlib.md5()
    myMd5.update(src.encode("utf8"))
    myMd5_Digest = myMd5.hexdigest()
    return myMd5_Digest

def random_number():
    '''
    随机生成11位数字
    '''
    x=random.randint(200000000000,999999999999)
    logger.debug('当前随机数为%s'%str(x))
    return str(x)

def random_fournumber():
    '''
    随机生成4位数字
    '''
    y = random.randint(3000,9999)
    logger.debug('当前随机数为%s'%(y))
    return y

def now_timestamp():
    '''
    获取当前10位时间戳
    '''
    now = int(time.time())
    return now


def encryption(date):
    '''
    传入入参，入参将进行加密并且生成sign
    '''
    list = []
    # 将入参循环拆解成字符串
    for key, value in sorted(date.items(), reverse=False):
        if key == 'sign':
            pass
        else:
            # 字符串重新拼接
            zz = "{}={}".format(key, value)
            list.append(zz)
    # 将字符串和秘钥拼接，字段以&分隔
    AuthToken = '&'.join(list) + "xxx"
    # print(AuthToken.encode('utf-8'))
    # 将入参加密
    sign = get_md5(AuthToken)
    # 加密后，将新的sign塞回入参内
    date['sign'] = sign
    return date


def tcp_conn():

    test = socket(AF_INET, SOCK_STREAM)
    test.connect((ip, int(port)))
    test.setsockopt(SOL_SOCKET, SO_KEEPALIVE, 1)  # 在客户端开启心跳维护
    test.settimeout(30)
    return test


def tcpconn_close():
    tcp_conn().close()


def sendmsg(conn, msg):
    '''
    发送序列化后的消息，返回服务器recv
    :param conn: 传入请求的socket套件
    :param msg:
    :return:服务器回包
    '''
    # conn.setblocking(0)   #压测时要注释这个代码，解除阻塞模式执行压测脚本
    a = True
    while a:

        conn.send(msg)
        time.sleep(3)
        try:
            recvmsg = conn.recv(4096)
            if recvmsg == '':
                logger.error('服务器返回空数据了')
                break
            elif len(recvmsg) > 0:
                logger.debug('回包的recvmsg长度大于0')
                a = False
        except OSError as e:
            logger.error('出现报错，%s' % e)
            conn.close()
    conn.setblocking(1)
    return recvmsg


def notifyrecvmsg(conn):
    '''
    该方法不发送消息，只做接收操作，用于接收notify消息推送
    :param conn: 传入请求的socket套件
    :return:服务器回包
    '''
    conn.setblocking(0)
    a = True
    while a:
        time.sleep(1)
        try:
            recvmsg = conn.recv(4096)
            if recvmsg == b'':
                logger.error('服务器返回空数据了')
                break
            elif len(recvmsg) > 0:
                logger.debug('回包的recvmsg长度大于0')
                a = False
        except OSError as e:
            logger.error('出现报错，%s' % e)
            conn.close()
    conn.setblocking(1)
    return recvmsg


def presssend(CmdId, SeqId, proto):
    '''
    处理协议请求，序列化入参
    :param ip: 请求的服务器ip地址
    :param port: 请求的服务器端口
    :param proto: 传入的协议名称
    :return:经过处理的待请求的消息
    '''
    csPKGHead = cs_head_pb2.CSHead()
    csPKGHead.CmdId = CmdId
    csPKGHead.SeqId = SeqId
    csPKG = cs_head_pb2.CSPKG()
    csPKG.Head.CopyFrom(csPKGHead)
    csPKG.Body = proto.SerializeToString()
    csPKGBuffer = csPKG.SerializeToString()
    CSPKG_nf = cs_head_pb2.CSPKG_nf()
    CSPKG_nf.Head.CopyFrom(csPKGHead)
    CSPKG_nf.Body = proto.SerializeToString()
    CompressType = cs_enum_pb2.ENM_COMPRESS_TYPE_NONE
    EncryptType = cs_enum_pb2.ENM_ENCRYPT_TYPE_NONE
    ByteSize = int(len(csPKGBuffer))
    First4Byte = (ByteSize << 8) + (((CompressType << 4) & 0xf0) + EncryptType & 0x0f)
    pkglist = []
    packHead = pack('!i', First4Byte)
    pkglist.append(packHead)
    pkglist.append(csPKGBuffer)
    x = b''.join(pkglist)
    return x


def pressrecvsend(msg, proto):
    '''

    :param msg: 处理好的待发送服务器的消息，返回处理后的服务器回包（head和body）
    :param proto:
    :return:
    '''
    recvdata = []
    CSPKG_nf = cs_head_pb2.CSPKG_nf()
    recvdata.append(msg)
    msg_buffer = b''.join(recvdata)
    max_pack_size = 724288
    msg_buffer_len = len(recvdata)
    curr_offset = 0
    while True:
        curr_len = msg_buffer_len - curr_offset
        if curr_len < 4:
            recvdata.append(msg_buffer[curr_offset:msg_buffer_len])
        First4Byte = unpack('>i', msg_buffer[curr_offset:4 + curr_offset])[0]
        pack_size = 4 + (First4Byte >> 8)
        if pack_size <= 5 or pack_size > max_pack_size:
            logger.error("pack size exception, pack_size: %d" % pack_size)
        if curr_len < pack_size:
            recvdata.append(msg_buffer[curr_offset:msg_buffer_len])
        CompressType = ((First4Byte & 0xff) >> 4)
        EncryptType = ((First4Byte & 0xff) & 0x0f)
        szContent = msg_buffer[curr_offset + 4:curr_offset + pack_size]
        CSPKG_nf.ParseFromString(szContent)
        proto.ParseFromString(CSPKG_nf.Body)
        # print('proto===========+%s'%proto)
        return CSPKG_nf.Head, proto
    # if CSPKG_nf.Head.CmdId == 1002:
    #     if CSPKG_nf.Head.RetCode != 0:
    #         print('RetCode不为0，当前RetCode是:%s'%CSPKG_nf.Head.RetCode)
    #         return CSPKG_nf.Head.RetCode
    #     else:
    #         proto.ParseFromString(CSPKG_nf.Body)
    #         #logger.info('服务器回包内容是：%s'%proto)
    #         return proto
    ## @TODO 服务器回包解压缩和解密还没实现，现在是修改服务器配置跳过了这个逻辑


def verifycmdid(conn, msg,proto, rescmdid):
    '''
    用于校验回包的消息是否和客户端请求的协议匹配
    :param conn:传入socket套件
    :param msg:传入序列化后的请求入参消息
    :param proto:传入回包反序列化的proto协议
    :param rescmdid:传入回包proto协议对应的cmdid
    :return:
    '''

    while True:
        recvmsg = sendmsg(conn, msg)
        head, body = pressrecvsend(recvmsg, proto)
        if head.CmdId != rescmdid:
            pass
        else:
            break
    return head, proto

 ## @TODO 需要实现把发送A协议后所有的回包都接收下来存起来
# def get_recvlist(conn, msg):
#     '''
#     用于校验回包的消息是否和客户端请求的协议匹配
#     :param conn:传入socket套件
#     :param msg:传入序列化后的请求入参消息
#
#     :return:
#     '''
#     recv_list = []
#     recvmsg = sendmsg(conn,msg)
#     while True:
#         if recvmsg not in recv_list:
#             recvmsg = sendmsg(conn,msg)
#             recv_list.append(recvmsg)
#         else:
#             break
#     return recv_list

# print(cs_cmdvalue_pb2.EnmCmdValue.ECV_CSLoginReq)


def openWorkbook(excelFile,column,row,sheet=0):
        '''
        column:填写key的取值行数(填写excel内的实际行数）
        row：填写velue的取值行数(填写excel内的实际行数）
        默认读取表里的第一个sheet的内容，如果要修改，则传对应excel里的sheet的数字（从0开始）
        '''
        # 读取excel表的数据
        # excelFile = u'F:\kleins_client\BuildDataConfig\DataConfig\Common\wp_物品信息\wp_物品信息.xlsx'
        workbook = xlrd.open_workbook(excelFile)
        # 选取需要读取数据的那一页
        sheet = workbook.sheet_by_index(int(sheet))
        # 获得行数和列数
        rows = sheet.nrows
        cols = sheet.ncols
        # 创建一个数组用来存储excel中的数据
        p = []
        row=int(row)-1
        column=int(column)-1
        for i in range(row, rows):
            d = {}
            for j in range(0, cols):
                if len('%s' % sheet.cell(column, j).value)==0:
                    pass
                else:
                    q = '%s' % sheet.cell(column, j).value
                    d[q] = sheet.cell(i, j).value
            ap = []
            for k, v in d.items():
                if isinstance(v, float):  # excel中的值默认是float,需要进行判断处理，通过'"%s":%d'，'"%s":"%s"'格式化数组
                    ap.append('"%s":%d' % (k, v))
                else:
                    ap.append('"%s":"%s"' % (k, v))
            s = '{%s}' % (','.join(ap))  # 继续格式化
            p.append(s)
        t = '[%s]' % (','.join(p))  # 格式化
        data = json.dumps(t, ensure_ascii=False)
        excel_json=data.replace('\\','')[1:-1]
        x = json.loads(excel_json)
        workbook.release_resources()
        return x

def get_excelcontent(excelname,column,row,sheet=0):
    '''
    获取项目配置表内的excel内容，需要填写表名
    column:填写key的取值行数(填写excel内的实际行数）
    row：填写velue的取值行数(填写excel内的实际行数）
    默认读取表里的第一个sheet的内容，如果要修改，则传对应excel里的sheet的数字（从0开始）
    '''
    pro_excelpath = 'G:\kleins_client\BuildDataConfig\DataConfig\Common\\'+excelname+'\\'+excelname+'.xlsx'
    excel_json = openWorkbook(pro_excelpath, column, row,sheet)
    return excel_json

def get_redis(type,userid):
    '''
    目前只用作查询redis内key对应的velue，不做修改操作.传入key值的类型以及userid
    '''
    redis_cli = Redis(host=redis_host, port=6379,password=redis_pw, db=0)
    try:
        #因为想要实现通用化，所以先通过keys查询到key，再通过查询到的key去找对应的value，多了一步，但是可以适用于其他redis值的查找
        for i in range(10):
            if redis_cli.keys('*'+type+':'+str(userid)+'*') ==[]:
                logger.info('查询中......')
                time.sleep(1)
            else:
                codes=redis_cli.keys('*'+type+':'+str(userid)+'*')[0]
                code=redis_cli.get(codes)
                logger.info('验证码是%s'%code.decode('utf-8'))
                return (code.decode('utf-8'))
    except:
        logger.error('没有在redis内找到对应value，可能是失效了')

# update_proto()