#coding=utf-8
from Bingkolo_Protobuf_Api_Test.base.setting import *
from configparser import ConfigParser
from Bingkolo_Protobuf_Api_Test.base.Protocol import cs_cmdvalue_pb2

class ReadConfig:
    def __init__(self):
        self.cf = ConfigParser()
        self.cf.read(path())

    def get_par(self, param):
        '''
        获取配置文件内的HOST参数
        '''
        value = self.cf.get("HOST", param)
        return value

    def get_db(self, param):
        '''
        获取配置文件内的DATA_BASE参数
        '''
        value = self.cf.get("DATA_BASE", param)
        return value

    def get_redisdb(self, param):
        '''
        获取配置文件内的REDIS_BASE参数
        '''
        value = self.cf.get("REDIS_BASE", param)
        return value


parameter = ReadConfig()
env = parameter.get_par('env')
ip = parameter.get_par('server_ip')
port = parameter.get_par('server_port')
protopath = parameter.get_par('source_protocol_path')
platuri = parameter.get_par('platuri')
accUri = parameter.get_par('localProduct_accUri')

dbhost = parameter.get_db('localProduct_db_host')
dbuser = parameter.get_db('localProduct_db_user')
dbpttb = parameter.get_db('localProduct_pt_table')
dbgmtb = parameter.get_db('localProduct_acc_table')
redis_host = parameter.get_redisdb('final_release_pre_host')
redis_pw = parameter.get_redisdb('final_release_pre_pw')

