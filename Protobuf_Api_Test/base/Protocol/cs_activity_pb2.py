# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: cs_activity.proto
"""Generated protocol buffer code."""
from google.protobuf.internal import builder as _builder
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


import  Bingkolo_Protobuf_Api_Test.base.Protocol.cs_common_pb2


DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\x11\x63s_activity.proto\x12\x02\x63s\x1a\x0f\x63s_common.proto\"k\n\x10\x46ourteenSignInfo\x12\x1f\n\x04\x44\x61ys\x18\x01 \x03(\x0b\x32\x11.cs.CSSignDayData\x12\x0e\n\x06NowDay\x18\x02 \x02(\r\x12&\n\x06Status\x18\x03 \x02(\x0e\x32\x16.cs.EnumActivityStatus\"\x1a\n\x18\x43SGetFourteenSignInfoReq\">\n\x18\x43SGetFourteenSignInfoRes\x12\"\n\x04Info\x18\x01 \x02(\x0b\x32\x14.cs.FourteenSignInfo\"A\n\x1b\x43SGetFourteenSignInfoNotify\x12\"\n\x04Info\x18\x01 \x02(\x0b\x32\x14.cs.FourteenSignInfo\"$\n\x15\x43SSignFourteenSignReq\x12\x0b\n\x03\x44\x61y\x18\x01 \x02(\r\"9\n\x15\x43SSignFourteenSignRes\x12 \n\x08ItemList\x18\x01 \x03(\x0b\x32\x0e.cs.CSItemPair\"\x92\x01\n\x16SingleActivitySignInfo\x12\x0e\n\x06NowDay\x18\x01 \x02(\r\x12\x0f\n\x07NeedDay\x18\x02 \x02(\r\x12\x12\n\nActivityId\x18\x03 \x02(\r\x12!\n\x05State\x18\x04 \x02(\x0e\x32\x12.cs.EnmAwardStatus\x12 \n\x08ItemList\x18\x05 \x03(\x0b\x32\x0e.cs.CSItemPair\".\n\x18\x43SGetActivitySignInfoReq\x12\x12\n\nActivityId\x18\x03 \x01(\r\">\n\x18\x43SGetActivitySignInfoRes\x12\"\n\x04Info\x18\x01 \x02(\x0b\x32\x14.cs.ActivitySignInfo\"@\n\x10\x41\x63tivitySignInfo\x12,\n\x08SignInfo\x18\x01 \x03(\x0b\x32\x1a.cs.SingleActivitySignInfo\"A\n\x1b\x43SGetActivitySignInfoNotify\x12\"\n\x04Info\x18\x01 \x02(\x0b\x32\x14.cs.ActivitySignInfo\"<\n\x19\x43SGetActivitySignAwardReq\x12\x0b\n\x03\x44\x61y\x18\x01 \x02(\r\x12\x12\n\nActivityId\x18\x02 \x02(\r\"=\n\x19\x43SGetActivitySignAwardRes\x12 \n\x08ItemList\x18\x01 \x03(\x0b\x32\x0e.cs.CSItemPair\"\x1a\n\x18\x43SGetActivityTimeInfoReq\"P\n\x18\x43SGetActivityTimeInfoRes\x12\x34\n\x14\x41\x63tivityTimeInfoList\x18\x01 \x03(\x0b\x32\x16.cs.CSActivityTimeInfo\"J\n\x16\x43SActivityChangeNotify\x12\x30\n\x10\x41\x63tivityTimeInfo\x18\x01 \x01(\x0b\x32\x16.cs.CSActivityTimeInfo\"\x19\n\x17\x43SRechargeRebateInfoReq\"\x9d\x01\n\x17\x43SRechargeRebateInfoRes\x12\x1f\n\x06Orders\x18\x01 \x03(\x0b\x32\x0f.cs.CSOrderData\x12\x14\n\x0cMonthCardNum\x18\x02 \x01(\r\x12\x10\n\x08XiyinNum\x18\x03 \x01(\r\x12\'\n\x0b\x41wardStatus\x18\x04 \x02(\x0e\x32\x12.cs.EnmAwardStatus\x12\x10\n\x08LeftTime\x18\x05 \x01(\r\"\x18\n\x16\x43SGetRechargeRebateReq\"R\n\x16\x43SGetRechargeRebateRes\x12\x14\n\x0cMonthCardNum\x18\x01 \x01(\r\x12\x10\n\x08XiyinNum\x18\x02 \x01(\r\x12\x10\n\x08LeftTime\x18\x03 \x01(\r\"B\n!CSLimitedTimeActStateChangeNotify\x12\x0e\n\x06TimeId\x18\x01 \x01(\r\x12\r\n\x05State\x18\x02 \x01(\r\"y\n\x11\x43SBattlePassAward\x12\x11\n\tBPPayType\x18\x01 \x01(\r\x12\x0c\n\x04\x42PLv\x18\x02 \x01(\r\x12!\n\x05State\x18\x03 \x01(\x0e\x32\x12.cs.EnmAwardStatus\x12 \n\x08ItemList\x18\x04 \x03(\x0b\x32\x0e.cs.CSItemPair\"\x98\x02\n\x10\x43SBattlePassInfo\x12\x0c\n\x04\x42PID\x18\x01 \x01(\r\x12\r\n\x05\x42PExp\x18\x02 \x01(\r\x12\x0c\n\x04\x42PLv\x18\x03 \x01(\r\x12.\n\x0f\x42\x61ttlePassAward\x18\x04 \x03(\x0b\x32\x15.cs.CSBattlePassAward\x12\x0f\n\x07WeekExp\x18\x05 \x01(\r\x12\x0f\n\x07Paytype\x18\x06 \x01(\r\x12\x10\n\x08LeftTime\x18\x07 \x01(\r\x12%\n\x04\x42\x61se\x18\x08 \x01(\x0b\x32\x17.cs.CSRechargePointData\x12\'\n\x06Middle\x18\t \x01(\x0b\x32\x17.cs.CSRechargePointData\x12%\n\x04High\x18\n \x01(\x0b\x32\x17.cs.CSRechargePointData\"\x18\n\x16\x43SGetBattlePassInfoReq\"F\n\x16\x43SGetBattlePassInfoRes\x12,\n\x0e\x42\x61ttlePassInfo\x18\x01 \x01(\x0b\x32\x14.cs.CSBattlePassInfo\"<\n\x17\x43SGetBattlePassAwardReq\x12\x0c\n\x04\x42PLv\x18\x01 \x01(\r\x12\x13\n\x0b\x43ontrolType\x18\x02 \x01(\r\"i\n\x17\x43SGetBattlePassAwardRes\x12,\n\x0e\x42\x61ttlePassInfo\x18\x01 \x01(\x0b\x32\x14.cs.CSBattlePassInfo\x12 \n\x08ItemList\x18\x02 \x03(\x0b\x32\x0e.cs.CSItemPair\"F\n\x16\x43SBattlePassInfoNotify\x12,\n\x0e\x42\x61ttlePassInfo\x18\x01 \x01(\x0b\x32\x14.cs.CSBattlePassInfo\"+\n\x17\x43SBuyBattlePassLevelReq\x12\x10\n\x08\x42uyLevel\x18\x01 \x01(\r\"G\n\x17\x43SBuyBattlePassLevelRes\x12,\n\x0e\x42\x61ttlePassInfo\x18\x01 \x01(\x0b\x32\x14.cs.CSBattlePassInfo\"\x1e\n\x1c\x43SGetCommunityFollowAwardReq\"<\n\x1c\x43SGetCommunityFollowAwardRes\x12\x1c\n\x04Item\x18\x01 \x01(\x0b\x32\x0e.cs.CSItemPair\"\x1a\n\x18\x43SCommunityFollowInfoReq\"I\n\x18\x43SCommunityFollowInfoRes\x12\x1c\n\x04Item\x18\x01 \x01(\x0b\x32\x0e.cs.CSItemPair\x12\x0f\n\x07IsAward\x18\x02 \x01(\x08\"\x16\n\x14\x43SLimitedTimeInfoReq\"F\n\x14\x43SLimitedTimeInfoRes\x12.\n\x0fLimitedTimeInfo\x18\x01 \x03(\x0b\x32\x15.cs.CSLimitedTimeInfo')

_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, globals())
_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, 'cs_activity_pb2', globals())
if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  _FOURTEENSIGNINFO._serialized_start=42
  _FOURTEENSIGNINFO._serialized_end=149
  _CSGETFOURTEENSIGNINFOREQ._serialized_start=151
  _CSGETFOURTEENSIGNINFOREQ._serialized_end=177
  _CSGETFOURTEENSIGNINFORES._serialized_start=179
  _CSGETFOURTEENSIGNINFORES._serialized_end=241
  _CSGETFOURTEENSIGNINFONOTIFY._serialized_start=243
  _CSGETFOURTEENSIGNINFONOTIFY._serialized_end=308
  _CSSIGNFOURTEENSIGNREQ._serialized_start=310
  _CSSIGNFOURTEENSIGNREQ._serialized_end=346
  _CSSIGNFOURTEENSIGNRES._serialized_start=348
  _CSSIGNFOURTEENSIGNRES._serialized_end=405
  _SINGLEACTIVITYSIGNINFO._serialized_start=408
  _SINGLEACTIVITYSIGNINFO._serialized_end=554
  _CSGETACTIVITYSIGNINFOREQ._serialized_start=556
  _CSGETACTIVITYSIGNINFOREQ._serialized_end=602
  _CSGETACTIVITYSIGNINFORES._serialized_start=604
  _CSGETACTIVITYSIGNINFORES._serialized_end=666
  _ACTIVITYSIGNINFO._serialized_start=668
  _ACTIVITYSIGNINFO._serialized_end=732
  _CSGETACTIVITYSIGNINFONOTIFY._serialized_start=734
  _CSGETACTIVITYSIGNINFONOTIFY._serialized_end=799
  _CSGETACTIVITYSIGNAWARDREQ._serialized_start=801
  _CSGETACTIVITYSIGNAWARDREQ._serialized_end=861
  _CSGETACTIVITYSIGNAWARDRES._serialized_start=863
  _CSGETACTIVITYSIGNAWARDRES._serialized_end=924
  _CSGETACTIVITYTIMEINFOREQ._serialized_start=926
  _CSGETACTIVITYTIMEINFOREQ._serialized_end=952
  _CSGETACTIVITYTIMEINFORES._serialized_start=954
  _CSGETACTIVITYTIMEINFORES._serialized_end=1034
  _CSACTIVITYCHANGENOTIFY._serialized_start=1036
  _CSACTIVITYCHANGENOTIFY._serialized_end=1110
  _CSRECHARGEREBATEINFOREQ._serialized_start=1112
  _CSRECHARGEREBATEINFOREQ._serialized_end=1137
  _CSRECHARGEREBATEINFORES._serialized_start=1140
  _CSRECHARGEREBATEINFORES._serialized_end=1297
  _CSGETRECHARGEREBATEREQ._serialized_start=1299
  _CSGETRECHARGEREBATEREQ._serialized_end=1323
  _CSGETRECHARGEREBATERES._serialized_start=1325
  _CSGETRECHARGEREBATERES._serialized_end=1407
  _CSLIMITEDTIMEACTSTATECHANGENOTIFY._serialized_start=1409
  _CSLIMITEDTIMEACTSTATECHANGENOTIFY._serialized_end=1475
  _CSBATTLEPASSAWARD._serialized_start=1477
  _CSBATTLEPASSAWARD._serialized_end=1598
  _CSBATTLEPASSINFO._serialized_start=1601
  _CSBATTLEPASSINFO._serialized_end=1881
  _CSGETBATTLEPASSINFOREQ._serialized_start=1883
  _CSGETBATTLEPASSINFOREQ._serialized_end=1907
  _CSGETBATTLEPASSINFORES._serialized_start=1909
  _CSGETBATTLEPASSINFORES._serialized_end=1979
  _CSGETBATTLEPASSAWARDREQ._serialized_start=1981
  _CSGETBATTLEPASSAWARDREQ._serialized_end=2041
  _CSGETBATTLEPASSAWARDRES._serialized_start=2043
  _CSGETBATTLEPASSAWARDRES._serialized_end=2148
  _CSBATTLEPASSINFONOTIFY._serialized_start=2150
  _CSBATTLEPASSINFONOTIFY._serialized_end=2220
  _CSBUYBATTLEPASSLEVELREQ._serialized_start=2222
  _CSBUYBATTLEPASSLEVELREQ._serialized_end=2265
  _CSBUYBATTLEPASSLEVELRES._serialized_start=2267
  _CSBUYBATTLEPASSLEVELRES._serialized_end=2338
  _CSGETCOMMUNITYFOLLOWAWARDREQ._serialized_start=2340
  _CSGETCOMMUNITYFOLLOWAWARDREQ._serialized_end=2370
  _CSGETCOMMUNITYFOLLOWAWARDRES._serialized_start=2372
  _CSGETCOMMUNITYFOLLOWAWARDRES._serialized_end=2432
  _CSCOMMUNITYFOLLOWINFOREQ._serialized_start=2434
  _CSCOMMUNITYFOLLOWINFOREQ._serialized_end=2460
  _CSCOMMUNITYFOLLOWINFORES._serialized_start=2462
  _CSCOMMUNITYFOLLOWINFORES._serialized_end=2535
  _CSLIMITEDTIMEINFOREQ._serialized_start=2537
  _CSLIMITEDTIMEINFOREQ._serialized_end=2559
  _CSLIMITEDTIMEINFORES._serialized_start=2561
  _CSLIMITEDTIMEINFORES._serialized_end=2631
# @@protoc_insertion_point(module_scope)
