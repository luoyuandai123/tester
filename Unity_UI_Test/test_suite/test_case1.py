# _*_coding:utf-8 _*_
import os, sys, pytest, collections

curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(curPath)[0]
sys.path.append(rootPath)
from auto_ui.module.dorm import *
from auto_ui.module.platform_mail import *
from auto_ui.module.contract import *

commApi = commApi()
generalApi = generalApi()


class Test_suite_role():
    """角色相关测试用例"""

    @pytest.fixture(scope='class')
    def exceljson(self):
        u"""初始化从excel内拿取数据
        return:舍友名称对应舍友ID的dict
        return:角色对话信息表转化后的json"""
        js_rolejson = commApi.get_excelcontent('yc_养成配置表', '5', '6')
        rolelist = []
        rolenamelist = []
        print(js_rolejson)
        jsdh_rolespeakjson = commApi.get_excelcontent('jsdh_角色对话信息表', '5', '6', '1')
        for x in range(len(js_rolejson)):
            # 循环从表内拿取舍友ID，拿取的过程中去掉不可分配的舍友ID
            if js_rolejson[x]['1，不能分配房间n-1，不能分配驾驶舱n0，都不能分配'] == 1:
                pass
            else:
                rolelist.append(js_rolejson[x]['角色ID'])
                rolenamelist.append(js_rolejson[x]['角色名称（角色的名字与ID对应关系参照js_角色信息表）'].strip())

        roledict = dict(zip(rolenamelist, rolelist))
        logger.debug('舍友名称对应ID的列表为：%s'%roledict)
        return roledict, jsdh_rolespeakjson

    # @pytest.mark.skip('暂时不需要执行')
    @pytest.mark.parametrize('roommate',(['芙丝忒','云山晓月','纳娅兰','情绪','娜塔莉','赫德拉','帕瑞黛儿','埃克提可','极乐','马埃尔','艾雯','恒','易寻欢','帕丝忒','可可','克莱提卡','维克康妮','云山天荀','威廉','赛丽娜','小蜗','山栀','云桃','奈奈','琢木','达妮雅','引薪','长安','蔓泽兰']))
    def test_qiaomen(self, exceljson, roommate):
        u'''验证舍友在宿舍走廊的敲门对话'''
        roledict  = exceljson[0]
        jsdh_rolespeakjson = exceljson[1]
        roleid=roledict[roommate]
        speaklist, actualspeaklist = corridor_speak(jsdh_rolespeakjson,roleid, roommate)
        assert collections.Counter(speaklist) == collections.Counter(actualspeaklist)

    @pytest.mark.skip('暂时不需要执行')
    def test_fenpei(self):
        u'''分配舍友'''
        assign_roles()
        # assert collections.Counter(speaklist) == collections.Counter(actualspeaklist)

    def test_GmMail_playerOffline(self):
        '''
        玩家不在线的情况下发送全服邮件，登录后查收
        '''
        commApi.back_login()
        id1num,id2num,id3num,title,content=sendGmMail('1','0')
        commApi.olduser_setup('51167611183','11111111')
        before_ge,before_yin,before_xiyin,after_ge,after_yin,after_xiyin,mailtitle,mailcontent=verify_GmMail()
        assert title==mailtitle
        assert content==mailcontent
        assert int(before_ge)+id2num==int(after_ge)
        assert int(before_yin)+id3num==int(after_yin)
        assert int(before_xiyin)+id1num==int(after_xiyin)


    def test_GmMail_playerOnline(self):
        '''
        玩家在线的情况下发送全服邮件
        '''
        commApi.back_login()
        commApi.olduser_setup('51167611183','11111111')
        id1num,id2num,id3num,title,content=sendGmMail('1','0')
        before_ge,before_yin,before_xiyin,after_ge,after_yin,after_xiyin,mailtitle,mailcontent=verify_GmMail()
        assert title==mailtitle
        assert content==mailcontent
        assert int(before_ge)+id2num==int(after_ge)
        assert int(before_yin)+id3num==int(after_yin)
        assert int(before_xiyin)+id1num==int(after_xiyin)


    def test_PlayerMail_playerOffline(self):
        '''
        玩家不在线的情况下发送单个玩家邮件，登录后查收
        '''
        commApi.back_login()
        id1num,id2num,id3num,title,content=sendGmMail('78938','1')
        commApi.olduser_setup('51167611183','11111111')
        before_ge,before_yin,before_xiyin,after_ge,after_yin,after_xiyin,mailtitle,mailcontent=verify_GmMail()
        assert title==mailtitle
        assert content==mailcontent
        assert int(before_ge)+id2num==int(after_ge)
        assert int(before_yin)+id3num==int(after_yin)
        assert int(before_xiyin)+id1num==int(after_xiyin)


    def test_PlayerMail_playerOnline(self):
        '''
        玩家在线的情况下发送单个玩家邮件
        '''
        commApi.back_login()
        commApi.olduser_setup('51167611183','11111111')
        id1num,id2num,id3num,title,content=sendGmMail('78938','1')
        before_ge,before_yin,before_xiyin,after_ge,after_yin,after_xiyin,mailtitle,mailcontent=verify_GmMail()
        assert title==mailtitle
        assert content==mailcontent
        assert int(before_ge)+id2num==int(after_ge)
        assert int(before_yin)+id3num==int(after_yin)
        assert int(before_xiyin)+id1num==int(after_xiyin)


    def test_PlayerMail_Otherplayer(self):
        '''
        发送指定玩家邮件后，使用不同的账号查收邮件
        '''
        commApi.back_login()
        #登录B账号
        commApi.olduser_setup('13422221111','11111111')
        #给A账号发送邮件
        id1num,id2num,id3num,title,content=sendGmMail('78938','1')
        #校验B账号是否有收到邮件
        before_ge,before_yin,before_xiyin,after_ge,after_yin,after_xiyin,mailtitle,mailcontent=verify_GmMail()
        assert title!=mailtitle
        assert content!=mailcontent
        commApi.back_login()
        #登录A账号
        commApi.olduser_setup('51167611183','11111111')
        #校验A账号是否有收到邮件
        before_ge1,before_yin1,before_xiyin1,after_ge1,after_yin1,after_xiyin1,mailtitle1,mailcontent1=verify_GmMail()
        assert title==mailtitle1
        assert content==mailcontent1
        assert int(before_ge1)+id2num==int(after_ge1)
        assert int(before_yin1)+id3num==int(after_yin1)
        assert int(before_xiyin1)+id1num==int(after_xiyin1)


    def test_paomadeng(self):
        '''
        验证跑马灯在有效时间内是否正确展示
        '''
        # 登录账号A，创建公告
        A_token = platformApi().gm_login('13422221111', '123456')
        createNotice_json, platcontent = platformApi().get_createNotice(A_token, '2')
        featureId = createNotice_json['data']['id']
        # 公告提交审核
        platformApi().get_createApproval(A_token, '2', featureId, '1')
        # 登录账号B，审核公告
        B_token = platformApi().gm_login('13428750942', '123456')
        ApprovalList_json = platformApi().get_ApprovalList(B_token, '2')
        id = ApprovalList_json['data']['list'][0]['ID']
        updateApproval_json = platformApi().get_updateApproval(B_token, id, featureId, '4', '2')
        # 发布公告
        sendNotice_json, sendNotice_text = platformApi().get_sendNotice(A_token, featureId, '1000')
        sendNotice_Expectjson = {"code": 0, "data": "发送成功", "msg": "操作成功"}
        assert sendNotice_json == sendNotice_Expectjson, sendNotice_text
        logger.debug('尝试获取跑马灯内容')
        for i in range(5):
            if poco("decorationLayer(Clone)").child("view_urgent_notice_ui(Clone)").child("BG").child("Mask").child(
                    "TextNotice").exists():
                content = poco("decorationLayer(Clone)").child(
                    "view_urgent_notice_ui(Clone)").child("BG").child("Mask").child("TextNotice").get_text()
                logger.debug('成功展示跑马灯,跑马灯的内容是：%s' % content)
                break
            else:
                time.sleep(1)
                logger.debug('还未展示跑马灯')
        # 登录账号A，提交删除操作的审核
        platformApi().get_createApproval(A_token, '2', featureId, '2')
        # 登录账号B，审核公告(还原数据)
        ApprovalList_json1 = platformApi().get_ApprovalList(B_token, '2')
        id = ApprovalList_json1['data']['list'][0]['ID']
        updateApproval_json = platformApi().get_updateApproval(B_token, id, featureId, '4', '2')
        # 发布公告(还原数据)
        sendNotice_json1, sendNotice_text1 = platformApi().get_sendNotice(A_token, featureId, '1000')
        sendNotice_Expectjson1 = {"code": 0, "data": "发送成功", "msg": "操作成功"}
        assert sendNotice_json1 == sendNotice_Expectjson1, sendNotice_text1
        assert platcontent==content

    def test_start_contract(self):
        '''
        常规卡池抽卡
        '''
        commApi.back_login()
        accid=commApi.olduser_setup('51167611185', '11111111')
        in_contract()
        start_contract('1','2',accid)


    def test_start_newcontract(self):
        '''
        新账号在新手卡池内进行抽卡
        '''
        commApi.back_login()
        accid=commApi.newuser_setup()
        in_contract()
        commApi.newuser_contract()
        start_contract('1','3',accid)