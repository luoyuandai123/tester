# _*_coding:utf-8 _*_
import pytest,os,sys,time
curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(curPath)[0]
sys.path.append(rootPath)
from pytest import ExitCode



if __name__ == '__main__':
    now = time.strftime('%Y-%m-%d %H-%M-%S')
    pytest.main(["-s", "-vv",'../test_suite/test_case1.py'   #::Test_suite为指定运行某一个类内的测试用例
                 ,"--html=./report/result.html"])  #为了分享样式不丢失，追加--self-contained-html
    # pytest.main(["-s", "../Test_Suite/test_case1.py::Test_suite", "--pytest_report", "./report/%s result.html"%now])

