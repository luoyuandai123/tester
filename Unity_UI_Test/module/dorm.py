# _*_coding:utf-8 _*_
import time,re

from auto_ui.base.common import *
from auto_ui.base.Old_kedaApi import *
from auto_ui.base.in_ffmpeg import *
import collections

def assign_roles(is_verify_video=False):
    '''
    进入宿舍并且分配舍友到房间内
    '''
    excel_json = commApi().get_excelcontent('yc_养成配置表','4', '6')
    rolelist = []
    for x in range(len(excel_json)):
        # 循环从表内拿取舍友ID，拿取的过程中去掉不可分配的舍友ID
        if excel_json[x]['formal_role'] == 1:
            pass
        else:
            rolelist.append(excel_json[x]['id'])
    logger.debug('舍友列表为%s' % rolelist)

    logger.debug('尝试进入宿舍分配页面')
    poco("home_view(Clone)").child("Main").child("Left").child("MainRoomBtn").click()
    for foor in range(4):
        foor += 1
        if len(rolelist) == 0:
            logger.debug('所有舍友都分配完了')
            break
        else:
            pass
        logger.debug('当前楼层是：%s' % str(foor))
        if poco("view_role_room_ui(Clone)").child("Frame").child("FloorFrame").child("BtnList").child(
                "BtnFoor0" + str(foor)).child("BG").exists():
            poco("view_role_room_ui(Clone)").child("Frame").child("FloorFrame").child("BtnList").child(
                "BtnFoor0" + str(foor)).child("BG").click()
            poco("view_role_room_ui(Clone)").child("Frame").child("RoomFrame").child("RoleFrame").child(
                "RoleItem1").child("HeadBG").click()
            logger.debug('成功进入%s楼的舍友分配页面' % str(foor))
        else:
            time.sleep(1)
            logger.debug('等待进入舍友分配页面中')

        for i in range(10):
            i += 1
            logger.debug('当前房间号是%s' % i)
            if poco("view_role_room_ui(Clone)").child("Frame").child("RoomFrame").child("RoleFrame").child(
                    "RoleItem" + str(i)).child("HeadBG").child("HeadIcon").exists():
                logger.debug('当前房间号%s已经有人入住了' % i)
                continue
            else:
                poco("view_role_room_ui(Clone)").child("Frame").child("RoomFrame").child("RoleFrame").child(
                    "RoleItem" + str(i)).child("HeadBG").click()
                logger.debug('已选中房间号%s' % i)
            for r in rolelist:
                logger.debug('当前尝试分配的舍友ID是：%s' % str(r))
                for visible in range(3):
                    logger.debug('尝试分配舍友%s' % r)
                    if poco("view_role_room_ui(Clone)").child("Frame").child("FilterFrame").child("MiddleFrame").child(
                            "Viewport").child("Content").child(str(r)).exists() == False:
                        logger.debug('当前列表里没有舍友%s' % r)
                        x, y = poco("view_role_room_ui(Clone)").child("Frame").child(
                            "FilterFrame").child("MiddleFrame").child("Scrollbar").child("Sliding Area").child(
                            "Handle").get_position()
                        poco.swipe([x, y], [x, y + 0.15], duration=0.5)
                    else:
                        logger.debug('找到舍友%s了' % r)
                        break
                if poco("view_role_room_ui(Clone)").child("Frame").child("FilterFrame").child("MiddleFrame").child(
                        "Viewport").child("Content").child(str(r)).child("State03").exists() == True or \
                        poco("view_role_room_ui(Clone)").child("Frame").child("FilterFrame").child("MiddleFrame").child(
                            "Viewport").child("Content").child(str(r)).child("State01").exists() == True:
                    logger.debug('当前舍友的状态为不可分配%s' % r)
                    rolelist.remove(r)
                    logger.debug('把该舍友%s从列表里去掉' % r)
                    x, y = poco("view_role_room_ui(Clone)").child("Frame").child(
                        "FilterFrame").child("MiddleFrame").child("Scrollbar").child("Sliding Area").child(
                        "Handle").get_position()
                    poco.swipe([x, y], [x, y - 0.5], duration=0.5)
                    logger.debug('重置列表位置')
                elif poco("view_role_room_ui(Clone)").child("Frame").child("FilterFrame").child("MiddleFrame").child(
                        "Viewport").child("Content").child(str(r)).child("State02").exists() == True :
                    logger.debug('当前舍友的状态为已分配%s' % r)
                    rolelist.remove(r)
                    logger.debug('把该舍友%s从列表里去掉' % r)
                    x, y = poco("view_role_room_ui(Clone)").child("Frame").child(
                        "FilterFrame").child("MiddleFrame").child("Scrollbar").child("Sliding Area").child(
                        "Handle").get_position()
                    poco.swipe([x, y], [x, y - 0.5], duration=0.5)
                    logger.debug('重置列表位置')
                else:
                    logger.debug('当前舍友的状态为可分配%s' % r)
                    poco("view_role_room_ui(Clone)").child("Frame").child("FilterFrame").child("MiddleFrame").child(
                        "Viewport").child("Content").child(str(r)).click()
                    btn_text = poco("view_role_room_ui(Clone)").child("Frame").child("FilterFrame").child(
                        "OperationFrame").child("Sure").child("TextState").get_text()
                    if btn_text == '安排入住':
                        logger.debug('尝试安排舍友%s进入房间%s' % (r, i))
                        poco("view_role_room_ui(Clone)").child("Frame").child("FilterFrame").child(
                            "OperationFrame").child("Sure").child("TextState").click()
                        poco("decorationLayer(Clone)").child("dialogs").child("message_2btn_dialog(Clone)").child(
                            "frame").child("layout").child("opt").child("sure_txt").click()
                        #是否开启语音验证
                        if is_verify_video==True:
                            voicepath=commApi().get_viocepath()
                            if voicepath != '当前没有正在播放的语音':
                                speak=commApi().audio_recognition(voicepath)
                            else:
                                logger.error('当前舍友的这段文本没有播放语音或者语音异常')
                                EOFError
                        else:
                            pass
                    else:
                        logger.error('当前分配按钮不是安排入住，是%s' % btn_text)
                    if poco("view_role_room_ui(Clone)").child("Frame").child("RoomFrame").child("RoleFrame").child(
                            "RoleItem" + str(i)).child("HeadBG").child("HeadIcon").exists() == True \
                            and poco("view_role_room_ui(Clone)").child("Frame").child("FilterFrame").child(
                        "MiddleFrame").child("Viewport").child("Content").child(str(r)).child(
                        "State01").exists() == True:
                        logger.debug('分配成功')
                        rolelist.remove(r)
                        logger.debug('把该舍友%s从列表里去掉' % r)
                        break
                    else:
                        logger.error('当前楼层%s的宿舍%s没有分配成功' % (str(foor), i))
        if i == 10:
            logger.debug('当前楼层所有房间都住满了')
            poco("view_role_room_ui(Clone)").child("Frame").child("RoomFrame").child("RoleFrame").click()
            continue
        else:
            pass

def corridor_speak(rolespeak_json,roleid,roommate,is_verify_video=False):
    '''
    宿舍走廊触发对话，传入舍友名称
    '''
    speaklist=[]
    for speak in range(len(rolespeak_json)):
        if rolespeak_json[speak]['对应角色']==roommate and \
        rolespeak_json[speak]['场景idn1 = 驾驶舱n2 = 房间n3 = 音效列表专用n4 = 走廊对话n5 = 抽到角色对话']==4 and rolespeak_json[speak]['场景idn1 = 驾驶舱n2 = 房间n3 = 音效列表专用n4 = 走廊对话n5 = 抽到角色对话']==4:
            speaklist.append(rolespeak_json[speak]['内容文本'])
        else:
            pass
    logger.info('当前舍友的预期对话列表是%s' % speaklist)
    logger.debug('尝试进入%s所在房间的走廊' % roommate)
    commApi().sendgm('#enter -p corridor '+str(roleid))
    # poco("view_role_room_ui(Clone)").child("Frame").child("RoomFrame").child("RoleFrame").child("RoleItem"+str(i)).child("SelectBG").child("BtnVisit").click()
    for y in range(3):
        if poco("view_room_ui(Clone)").child("Rocker").child("TouchBg").child("RockerBg").exists():
            logger.debug('走廊加载成功，并且展示了房间的交互按钮')
            break
        else:
            time.sleep(1)
            logger.debug('等待走廊加载中...')
    actualspeaklist = []
    for i in range(25):
        i += 1
        logger.debug('====================================================当前第%s次触发敲门对话========================================================================'%i)
        if collections.Counter(speaklist)==collections.Counter(actualspeaklist):
            logger.debug('舍友的所有敲门对话都验证完成')
            break
        elif len(speaklist)==len(actualspeaklist) or len(speaklist)<len(actualspeaklist):
            logger.error('舍友敲门对话和表内预期对话不一致')
            EOFError
            break
        else:
            for gate in range (10):
                gate+=1
                if gate<int(10):
                    Gate="Gate0"+str(gate)
                else:
                    Gate = "Gate" + str(gate)
                if poco("SceneLoader").child("room_corridor_EditAsset(Clone)").child("GAMEPLAY").child("WorldUIRoot").child(
                    "GateButtons").child(Gate).exists():

                    poco("SceneLoader").child("room_corridor_EditAsset(Clone)").child("GAMEPLAY").child("WorldUIRoot").child(
                    "GateButtons").child(Gate).click()
                    break
                else:
                    logger.debug('咚咚咚，正在敲门中...')
            if poco("view_room_corridor_inter_ui(Clone)").child("Frame").child("Bg").child("TextTalk").exists():
                logger.debug('获取触发交互后的角色对话文本')
                actualspeak = poco("view_room_corridor_inter_ui(Clone)").child("Frame").child("Bg").child(
                    "TextTalk").get_text()
                actualFriendlevel = poco("view_room_corridor_inter_ui(Clone)").child("Frame").child(
                    "Bg").child("HeadBG").child("TextGoodLevel").get_text()
                logger.info('实际触发的对话是:%s,当前舍友好感度等级是:%s' % (actualspeak, actualFriendlevel))
                if actualspeak in actualspeaklist:
                    logger.debug('这句对话已经触发并且记录，无需再次验证')
                    pass
                else:
                    actualspeaklist.append(actualspeak)
                    # 是否开启语音验证
                    if is_verify_video==True:
                        voicepath=commApi().get_viocepath()
                        if voicepath != '当前没有正在播放的语音':
                            speak=commApi().audio_recognition(voicepath)
                        else:
                            logger.error('当前舍友的这段文本没有播放语音或者语音异常')
                            EOFError
                    else:
                        pass
                    try:
                        assert actualspeak in speaklist
                    except:
                        logger.error('实际对话结果和表内的配置不一致，|%s| Not in |%s|' % (actualspeak, speaklist))
                logger.debug('尝试返回走廊页面')
                poco("BtnBack").click()
            else:
                logger.error('没有获取到舍友对话文本信息')
        logger.debug('当前舍友的实际触发的对话列表是%s' % actualspeaklist)
    commApi().back_home()
    return actualspeaklist,speaklist




# corridor_speak('帕瑞黛儿',True)

def corridor_robit():
    if poco("view_gm_ui(Clone)").child("GMLogTools").child("TextInfo").exists():
        pass
    else:
        commApi().sendgm('#OpenRoomDisShow')
    for x in range(10):
        commApi().sendgm('#enter -p corridor')
        for i in range(3):
            if poco("uiroot_f1").child("UI").child("view_room_ui(Clone)").exists():
                logger.debug('走廊加载成功')
                break
            else:
                time.sleep(0.5)
                logger.debug('正在等待走廊加载')
        if poco("SceneLoader").child("room_corridor_EditAsset(Clone)").child("GAMEPLAY").child("Airship").child(
            "RobotNode").child("robot(Clone)").exists():
            logger.debug('当前走廊内有机器人在走动')
            break
        else:
            logger.debug('当前走廊内没有机器人')
            commApi().sendgm('#LoadRobot')
            break
            # commApi().back_home()

    for xyz in range(20):
        if poco("SceneLoader").child("room_corridor_EditAsset(Clone)").child("GAMEPLAY").child("Airship").child(
                "RobotNode").child("Talk").exists():
            break
        else:
            before = poco("view_gm_ui(Clone)").child("GMLogTools").child("TextInfo").get_text()
            beforposition = re.split('\n|\：|\,', before)
            # beforjuli = beforposition[-1]
            # jt_x = beforposition[4]
            # logger.debug('jt_x%s' % jt_x)
            # jt_y = beforposition[5]
            # logger.debug('jt_y%s' % jt_y)
            jqr_x = beforposition[1]
            jqr_y = beforposition[2]
            logger.debug('机器人的位置是：(X:%s,Y:%s)' % (jqr_x,jqr_y))
            time.sleep(1)
            if float(jqr_x)==0.70 and float(jqr_y)==0.00:
                logger.debug('机器人出生在中间右侧')
                rockerx, rockery = poco("view_room_ui(Clone)").child("Rocker").child("TouchBg").child("RockerBg").child(
                    "Point").get_position()
                poco.swipe([rockerx, rockery], [rockerx, rockery - 3], duration=0.1)
                pagex, pagey = poco("view_room_ui(Clone)").child("DragFrame").child("Left").get_position()

                poco.swipe([pagex, pagey], [pagex , pagey + 0.4], duration=0.01)

            elif float(jqr_x)==-0.70 and float(jqr_y)==0.00:
                logger.debug('机器人出生在中间左侧')
                rockerx, rockery = poco("view_room_ui(Clone)").child("Rocker").child("TouchBg").child("RockerBg").child(
                    "Point").get_position()
                poco.swipe([rockerx, rockery], [rockerx, rockery - 3], duration=0.1)
                rockerx, rockery = poco("view_room_ui(Clone)").child("Rocker").child("TouchBg").child("RockerBg").child(
                    "Point").get_position()
                poco.swipe([rockerx, rockery], [rockerx-1, rockery], duration=0.1)
                pagex, pagey = poco("view_room_ui(Clone)").child("DragFrame").child("Left").get_position()
                #镜头下拉一些
                poco.swipe([pagex, pagey], [pagex , pagey + 0.4], duration=0.01)
                time.sleep(3)

            elif 0.2<float(jqr_x) <= 0.70 and float(jqr_y) == 10.82:
                logger.debug('机器人出生在前方右侧')
                rockerx, rockery = poco("view_room_ui(Clone)").child("Rocker").child("TouchBg").child("RockerBg").child(
                    "Point").get_position()
                poco.swipe([rockerx, rockery], [rockerx + 0.1, rockery], duration=0.0001)
                #往前移动
                rockerx, rockery = poco("view_room_ui(Clone)").child("Rocker").child("TouchBg").child("RockerBg").child(
                    "Point").get_position()
                poco.swipe([rockerx, rockery], [rockerx, rockery - 9.5], duration=1)
                logger.debug('等待机器人走过来...')
                pagex, pagey = poco("view_room_ui(Clone)").child("DragFrame").child("Left").get_position()
                #镜头下拉一些
                poco.swipe([pagex, pagey], [pagex , pagey + 0.4], duration=0.01)
                time.sleep(3)

            elif -0.2>float(jqr_x) >= -0.70 and float(jqr_y) == 10.82:
                logger.debug('机器人出生在前方左侧')
                rockerx, rockery = poco("view_room_ui(Clone)").child("Rocker").child("TouchBg").child("RockerBg").child(
                    "Point").get_position()
                poco.swipe([rockerx, rockery], [rockerx + 0.1, rockery], duration=0.0001)
                rockerx, rockery = poco("view_room_ui(Clone)").child("Rocker").child("TouchBg").child("RockerBg").child(
                    "Point").get_position()
                poco.swipe([rockerx, rockery], [rockerx, rockery - 9.5], duration=1)
                logger.debug('等待机器人走过来...')
                pagex, pagey = poco("view_room_ui(Clone)").child("DragFrame").child("Left").get_position()
                poco.swipe([pagex, pagey], [pagex , pagey + 0.4], duration=0.01)
                time.sleep(3)
            elif float(jqr_x) == 0.70 and -8>float(jqr_y) >= -10.82:
                logger.debug('机器人出生在背后右侧')
                pagex, pagey = poco("view_room_ui(Clone)").child("DragFrame").child("Left").get_position()
                poco.swipe([pagex, pagey], [pagex + 0.3, pagey + 0.4], duration=0.01)
            elif float(jqr_x) == -0.70 and -8>float(jqr_y) >= -10.82:
                logger.debug('机器人出生在背后左侧')
                pagex, pagey = poco("view_room_ui(Clone)").child("DragFrame").child("Left").get_position()
                poco.swipe([pagex, pagey], [pagex - 0.3, pagey + 0.4], duration=0.01)
    # after = poco("decorationLayer(Clone)").child("view_gm_ui(Clone)").child("GMLogTools").child("TextInfo").get_text()
    # afterposition = re.split('\n|\：|\,', after)
    # afterjuli = afterposition[-1]
    # x = float(jt_x) - float(jqr_x)
    # print('xxxxxxxxxxx%s' % x)
    #
    # if float(beforjuli) - float(afterjuli) > 0:
    #     logger.debug('机器人在朝我驶来')
    #     if float(jt_x) < 0 and float(jqr_x) < 0 and xposition < 0.1:
    #         logger.debug('机器人基本和玩家视线齐平')
    #         if afterjuli < 0.1:
    #             logger.debug('好像差不多了，机器人快走到面前了')
    #             break
    #         elif float(jt_x) >= 0 and float(jqr_x) < 0:
    #             rockerx, rockery = poco("view_room_ui(Clone)").child("Rocker").child("TouchBg").child("RockerBg").child(
    #                 "Point").get_position()
    #             poco.swipe([rockerx, rockery], [rockerx + 0.1, rockery], duration=0.0001)
    #         elif float(jt_x) <= 0 and float(jqr_x) > 0:
    #             rockerx, rockery = poco("view_room_ui(Clone)").child("Rocker").child("TouchBg").child("RockerBg").child(
    #                 "Point").get_position()
    #             poco.swipe([rockerx, rockery], [rockerx + 0.1, rockery], duration=0.0001)
    # elif float(beforjuli) - float(afterjuli) == 0:
    #     logger.debug('机器人好像没有移动了')
    #     break
    # else:
    #     logger.debug('反了反了')
# corridor_robit()
    robotjson=commApi().get_excelcontent('ss_宿舍配置信息表','5','6','14')
    emo_namelist=[]
    xiao_list=[]
    zhayan_list=[]
    weiqu_list=[]
    xin_list=[]
    for emo in range(len(robotjson)):
        if robotjson[emo]['表情名称（公式列）'] =='fx_jiqiren_xiao':
            xiao_list.append(robotjson[emo]['对话内容备注'])
        elif robotjson[emo]['表情名称（公式列）'] =='fx_jiqiren_zhayan':
            zhayan_list.append(robotjson[emo]['对话内容备注'])
        elif robotjson[emo]['表情名称（公式列）'] == 'fx_jiqiren_weiqu':
            weiqu_list.append(robotjson[emo]['对话内容备注'])
        elif robotjson[emo]['表情名称（公式列）'] == 'fx_jiqiren_xin':
            xin_list.append(robotjson[emo]['对话内容备注'])
        else:
            logger.warning('居然有匹配不上的数据')
    for emo in range(len(robotjson)):
        if robotjson[emo]['表情名称（公式列）'] in emo_namelist:
            pass
        else:
            emo_namelist.append(robotjson[emo]['表情名称（公式列）'])
    logger.debug('机器人目前有%s个表情，每个表情各有%s条、%s条、%s条、%s条对话'%(len(emo_namelist),len(xiao_list),len(zhayan_list),len(weiqu_list),len(xin_list)))
    for i in range(1):
        i+=1
        logger.debug('====================================================当前第%s次和机器人触发交互========================================================================' % i)
        for i in range(3):
            if poco("SceneLoader").child("room_corridor_EditAsset(Clone)").child("GAMEPLAY").child("Airship").child(
                    "RobotNode").child("Talk").exists():
                poco("SceneLoader").child("room_corridor_EditAsset(Clone)").child("GAMEPLAY").child("Airship").child(
                    "RobotNode").child("Talk").click()
                break
            else:
                time.sleep(1)
                logger.debug('还没有触发交互按钮')
        for i in range(3):
            if poco("view_room_robot_inter_ui(Clone)").child("Frame").child("Bg").child(
                    "TextTalk").exists() == True:
                text = poco("view_room_robot_inter_ui(Clone)").child("Frame").child("Bg").child(
                    "TextTalk").get_text()
                logger.debug('获取对话文本')
                break
            else:
                    time.sleep(1)
        for emo in emo_namelist:
            try:
                if poco("SceneLoader").child("room_corridor_EditAsset(Clone)").child("GAMEPLAY").child("Airship").child(
                    "RobotNode").child("robot(Clone)").child("Root_node").child("Bone_zong_001").child("Bone_head_001").child(
                    "Expressions").child(emo).exists() and emo=='fx_jiqiren_xiao':
                    logger.debug('当前触发的是%s的对话,对话内容是：%s'%(emo,text))
                    assert  text in xiao_list,text
                    break
                elif poco("SceneLoader").child("room_corridor_EditAsset(Clone)").child("GAMEPLAY").child("Airship").child(
                    "RobotNode").child("robot(Clone)").child("Root_node").child("Bone_zong_001").child("Bone_head_001").child(
                    "Expressions").child(emo).exists() and emo=='fx_jiqiren_zhayan':
                    logger.debug('当前触发的是%s的对话,对话内容是：%s'%(emo,text))
                    assert  text in zhayan_list,text
                    break
                elif poco("SceneLoader").child("room_corridor_EditAsset(Clone)").child("GAMEPLAY").child("Airship").child(
                    "RobotNode").child("robot(Clone)").child("Root_node").child("Bone_zong_001").child("Bone_head_001").child(
                    "Expressions").child(emo).exists() and emo=='fx_jiqiren_weiqu':
                    logger.debug('当前触发的是%s的对话,对话内容是：%s'%(emo,text))
                    assert  text in weiqu_list,text
                    break
                elif poco("SceneLoader").child("room_corridor_EditAsset(Clone)").child("GAMEPLAY").child("Airship").child(
                    "RobotNode").child("robot(Clone)").child("Root_node").child("Bone_zong_001").child("Bone_head_001").child(
                    "Expressions").child(emo).exists() and emo=='fx_jiqiren_xin':
                    logger.debug('当前触发的是%s的对话,对话内容是：%s'%(emo,text))
                    assert  text in xin_list,text
                    break

            except:
                logger.error('对话')
        logger.debug('尝试返回走廊页面')
        if poco("view_room_ui(Clone)").child("view_top_back_ui").child("BtnBack").exists()== True:
            logger.debug('已经在走廊里了')
        else:
            poco("view_room_robot_inter_ui(Clone)").child("view_top_back_ui").child("BtnBack").click()


corridor_robit()