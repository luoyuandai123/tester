# _*_coding:utf-8 _*_
import time

from auto_ui.base.common import *
from  auto_ui.base.httpcommon import *

def in_contract():
    for i in range(3):
        if poco("home_view(Clone)").child("Right").child("DrawCard").exists():
            poco("home_view(Clone)").child("Right").child("DrawCard").click()
            break
        else:
            time.sleep(1)
            logger.debug('等待付费招募入口展示...')



def start_contract(num,Cardpool,accid):
    '''
    十连抽卡功能
    num:填写抽卡次数
    Cardpool：填写想抽的卡池，1：常规卡池，2:限定卡池
    accid：填写抽卡账号的accid
    '''
    start_time=generalApi().now_time()
    logger.debug('当次的开始时间是%s' % start_time)
    xiyou = [ 'R', 'SR', 'SSR']
    alllist = []
    for i in range(3):
        if poco("view_recruit_pay_ui(Clone)").child("RecruitBtns").child("Btn_"+str(int(Cardpool)-1)).exists():
            poco("view_recruit_pay_ui(Clone)").child("RecruitBtns").child("Btn_"+str(int(Cardpool)-1)).click()
            break
        else:
            time.sleep(1)
            logger.debug('等待卡池列表展示中...')
    for i in range(int(num)):
        logger.debug('本轮测试的第%s次抽卡开始'%(str(int(i)+1)))
        list=[]
        item1=poco("view_recruit_pay_ui(Clone)").child("RecruitMain").child("TopBtnA").child("Count").get_text()
        logger.debug('当前因的数量是%s'%item1)
        item2=poco("view_recruit_pay_ui(Clone)").child("RecruitMain").child("TopBtnB").child("Count").get_text()
        logger.debug('当前内隐记录的数量是%s'%item2)
        for i in range(4):
            logger.debug('尝试点击10抽按钮')
            if poco("view_recruit_pay_ui(Clone)").child("RecruitMain").child("Right").child("RecruitBtnTen").exists():
                logger.debug('10连抽卡按钮成功展示')
                poco("view_recruit_pay_ui(Clone)").child("RecruitMain").child("Right").child("RecruitBtnTen").click()
                break
            else:
                logger.debug('等待10连按钮展示中...')
                time.sleep(1)
        for i in range(5):
            logger.debug('尝试进入转球页面')
            if poco("view_recruit_ui(Clone)").child("Description").exists():
                logger.debug('成功进入转球页面')
                break
            else:
                logger.debug('等待转球页面加载中...')
                time.sleep(2)
        logger.debug('尝试开始转球球')
        if i >0:
            pass
        else:
            logger.debug('看样子要设置下转球的阻尼和速度了')
            commApi().sendgm('#set acclimit -p 10000')
            commApi().sendgm('#set dampingradio -p 0.1')
        poco.swipe([0.3, 0.462], [0.83, 0.462], duration=0.05)

        logger.debug('尝试点击抽卡翻牌地图页的跳过按钮')
        for i in range(3):
            if poco("view_recruit_map_ui(Clone)").child("Skip").exists():
                poco("view_recruit_map_ui(Clone)").child("Skip").click()
                logger.debug('跳过十连地图页')
                break
            else:
                logger.debug('等待跳过十连地图页面中...')
                time.sleep(2)
        for i in range(3):
            logger.debug('尝试等待展示十连结果页')
            if poco("view_multi_recruit_ui(Clone)").child("Description").exists():
                logger.debug('十连结果页正常展示')
                break
            else:
                logger.debug('等待展示十连结果页中...')
                time.sleep(1)
        if env == 'product' and Running_environment == 'local':
            pass
        else:
            logger.debug('尝试获取当次抽卡的舍友稀有度分布')
            for i in range(10):
                i += 1
                for x in xiyou:
                    if poco("view_multi_recruit_ui(Clone)").child("Frame").child("RoleCardParent" + str(i)).child("VFXGroup").child(x + "(Clone)").exists():
                        logger.debug('%s位置是%s卡' % (i, x))
                        list.append(x)
                        break
                    try:
                        zi = poco("view_multi_recruit_ui(Clone)").child("Frame").child("RoleCardParent" + str(i)).child(
                            "VFXGroup").children()
                        zy = zi.get_text()
                    except:
                        list.append('N')
                        logger.debug('%s位置是是N卡' % i)
                        break
                    else:
                        pass
            logger.debug('当次抽卡的结果是%s'%(list))

        alllist.extend(list)
        list.clear()
        logger.debug('尝试点击抽卡结果页的跳过按钮')
        poco("view_multi_recruit_ui(Clone)").child("Description").click()
        logger.debug('判断舍友获得后是否转化了道具...')
        if poco("view_common_get_items(Clone)").child("Description").child("Text").exists():
            logger.debug('当前舍友获得后有道具转化')
            poco("view_common_get_items(Clone)").child("Description").child("Text").click()
            logger.debug('关闭道具转化的获得页面')
        else:
            logger.debug('舍友获得后没转化道具')
            pass
    if env == 'product' and Running_environment == 'local':
        total_number = excute_mysql_str('select  count(*) from DBGacha_0 where Accid=%s and PoolType=\'%s\' and Star!=\'0\' and Time>=\'%s\';'%(accid,Cardpool,start_time))[0][0]
        SSR_number = excute_mysql_str('select  count(*) from DBGacha_0 where Accid=\'%s\' and PoolType=\'%s\' and Star=\'4\' and Time>=\'%s\';'%(accid,Cardpool,start_time))[0][0]
        SR_number = excute_mysql_str('select  count(*) from DBGacha_0 where Accid=\'%s\' and PoolType=\'%s\' and Star=\'3\' and Time>=\'%s\';'%(accid,Cardpool,start_time))[0][0]
        R_number = excute_mysql_str('select  count(*) from DBGacha_0 where Accid=\'%s\' and PoolType=\'%s\' and Star=\'2\' and Time>=\'%s\';'%(accid,Cardpool,start_time))[0][0]
        N_number = excute_mysql_str('select  count(*) from DBGacha_0 where Accid=\'%s\' and PoolType=\'%s\' and Star=\'1\' and Time>=\'%s\';'%(accid,Cardpool,start_time))[0][0]
        PoolType_number =  excute_mysql_str('select  count(*) from DBGacha_0 where Accid=\'%s\' and PoolType=\'%s\' and Star=\'1\';'%(accid,Cardpool))[0][0]
        logger.info("本次抽卡总抽卡次数为：%s次，结果为:N卡%s个，R卡%s个，SR卡%s个，SSR卡%s个,当前账号在卡池内总的抽卡次数为：%s"%(total_number,N_number,R_number,SR_number,SSR_number,PoolType_number))
        if Cardpool=='2':
            up_SSR_number=excute_mysql_str('select count(*) from DBGacha_0 where Accid = \'%s\' and PoolType = \'%s\' and Star = \'4\' and RoleID = \'1004\' and Time>=\'%s\';'%(accid,Cardpool,start_time))[0][0]
            logger.info("up角色的出货次数是%s次" % up_SSR_number)
        else:
            pass
    else:
        N=alllist.count('N')
        R=alllist.count('R')
        SR=alllist.count('SR')
        SSR=alllist.count('SSR')
        logger.info("本次抽卡结果为:N卡%s个，R卡%s个，SR卡%s个，SSR卡%s个,各稀有度分布情况是%s"%(N,R,SR,SSR,alllist))
        commApi().sendgm('~#showRecruitTimes '+Cardpool,'1')
        total_count=poco("decorationLayer(Clone)").child("view_gm_ui(Clone)").child("GMTools").child(
            "Frame").child("Cmd").child("Scroll View").child("Viewport").child("Content").child("CmdRecordLabel").get_text()
        total_count=total_count.split(': ')[-1]
        poco("GMBar").child("BtnGm").click()
        logger.info("当前账号在卡池%s的总抽卡次数是:%s"%(Cardpool,total_count))

