import os
from threading import Thread

# 在命令行窗口调用命令
def execute_cmd_command(source_audio, change_audio):
    '''
    ###########################################################
    注:因为是命令行窗口执行命令,因此以下路径要用绝对路径

    变量解释:
        source_audio:  需要更换格式的音频文件路径,这里测试选择m4a
        change_audio:  更换格式后的音频文件路径,这里测试选择wav
        ffmpeg_path :  ffmpeg.exe的文件路径,ffmpeg是音频转换重要文件,
                       下载官网在http://www.ffmpeg.org/download.html
                       windows的exe下载在这里https://github.com/BtbN/FFmpeg-Builds/releases
    ##########################################################
    '''
    # source_audio = input('输入需要更换格式的音频文件:')
    # change_audio = input('输入更换格式后的音频文件  :')
    ffmpeg_path = 'C:/ffmpeg-5.1.2-essentials_build/bin/ffmpeg.exe'

    # 执行文件转换的指令
    command ='@echo y|'+'ffmpeg.exe' + ' -i' + ' ' + source_audio + ' ' + change_audio

    os.system(str(command))
    # f=os.popen(str(command), 'r')
    # print(f.read())
    # 多线程运行指令
    # t = Thread(target = execute_cmd_command, args = (command,))
    # t.start()
# voicepath='role/paruidaier/outside/voice/audio_voice_paruidaier_accept_2'
# print(voicepath)
# change_audio = '../voice/' + voicepath.split('/')[-1] + '.wav'
# print('修改后的文件路径为%s' % change_audio)
# pro_path = 'F:/kleins_client/Assets/RAW/f1_audio/audio/'
# source_audio = pro_path + voicepath + '.ogg'
# execute_cmd_command(source_audio, change_audio)

