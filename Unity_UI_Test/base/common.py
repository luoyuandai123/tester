# _*_coding:utf-8 _*_
import io,sys
import time

from auto_ui.base.baseapi import *
import pyautogui



class commApi():
    '''
    游戏内业务相关方法
    '''
    def sendgm(self,content,isclose='0'):
        '''
        点击GM按钮发送GM命令
        content：填写需要发送的GM命令
        isclose:是否关闭GM弹窗，默认为关闭，如果填写字符串1，则不关闭
        '''
        poco("GMBar").child("BtnGm").click()
        poco("Cmd").child("InputCmd").click()
        pyautogui.typewrite(content)
        # pyautogui.hotkey("Enter")
        poco("Cmd").child("BtnSend").click()
        text=poco("decorationLayer(Clone)").child("view_gm_ui(Clone)").child("GMTools").child("Frame").child("Cmd").child("InputCmd").child("Text Area").child("Text").get_text()

        logger.info('输入GM命令完成，输入的GM命令是%s'%text.encode("utf-8",'ignore'))
        if isclose=='0':
            poco("GMBar").child("BtnGm").click()
        else:
            pass

    def newuser_setup(self):
        '''
        新注册账号-登录游戏-起名-跳过初始新手引导
        '''
        num = generalApi().random_number()
        logger.debug('当次使用的用户名是%s' % num)
        if poco("view_login_ui(Clone)").child("bg").child("GameLogo").exists():
            logger.debug('成功进入游戏登录页面')
            pass
        else:
            poco("view_login_ui(Clone)").child("bg").child("GameLogo").wait()
            logger.debug('等待进入游戏登录页面中...')
        if poco("RememberMainPage").child("ExitLoginBtn").exists():
            logger.debug('当前登录页面记住了上次登陆的账号密码')
            poco("RememberMainPage").child("ExitLoginBtn").click()
            logger.debug('正在退出上次登录的账号中...')
            time.sleep(1)
            poco("decorationLayer(Clone)").child("dialogs").child(
                "message_2btn_dialog(Clone)").child("frame").child("layout").child("opt").child("sure_txt").click()
            logger.debug('成功退出上次登录的账号')
        else:
            logger.debug('当前登录页面没有记录上次的玩家登录信息')
            pass
        logger.debug('点击登录页面，尝试登录')
        poco("view_login_ui(Clone)").child("bg").child("BgBtn").click()
        poco("MainPage").child("Mask").child("Content").child("LoginPage").child("AccountLogin").child(
            "IdInput").click()
        logger.debug('账号输入中...')
        pyautogui.typewrite(num)
        poco("AccountLogin").child("PsdInput").click()
        logger.debug('密码输入中...')
        pyautogui.typewrite('11111111')
        for i in range(5):
            logger.debug('尝试点击登录按钮进行登录')
            poco("LoginPage").child("AccountLogin").child("LoginBtn").child("LoginBtnRight").click()
            if poco("tips").child("message(Clone)").child("Content").exists():
                logger.debug('没有勾选是否同意隐私协议')
                poco("MainPage").child("Mask").child("Content").child(
                    "SecretToggleUp").child("Background").child("Checkmark").click()
                logger.debug('手动勾选隐私协议')
            else:
                logger.debug('隐私协议正常勾选状态，不需要再次勾选')
                break
        for i in range(5):
            logger.debug('开始进入新账号起名前的不可跳过剧情')
            if poco("plot_view(Clone)").child("AccBtn").exists():
                logger.debug('加速自动播放新账号不可跳过的剧情')
                poco("plot_view(Clone)").child("AccBtn").click()
                poco("plot_view(Clone)").child("AutoBtn").click()
                break
            else:
                poco("plot_view(Clone)").child("AccBtn").wait()
                logger.debug('等待不可跳过剧情展示中...')
                time.sleep(3)
        logger.debug('开始起名')
        poco("create_role_view(Clone)").child("NameInputBg").child("NameInput").child("Text Area").child("Text").click()
        logger.debug('名称输入中...')
        pyautogui.typewrite('0001')
        poco("create_role_view(Clone)").child("SureCreateRoleName").click()
        #现在没有新手引导，暂时注释
        # logger.debug('跳过新手引导的前置剧情')
        # poco("plot_view(Clone)").child("PassBtn").wait()
        # poco("plot_view(Clone)").child("PassBtn").click()
        # logger.debug('确定跳过新手引导的前置剧情')
        # poco("plot_view(Clone)").child("PassPanel").child("SureBtn").wait()
        # poco("plot_view(Clone)").child("PassPanel").child("SureBtn").click()
        # for i in range(3):
        #     logger.debug('尝试开始输入GM命令')
        #     if poco("guide_view(Clone)").child("TxtContent").exists() or poco("GMBar").child("BtnGm").exists():
        #         logger.debug('GM窗口正常显示，可以开始输入GM')
        #         commApi().sendgm('~#finishguide')
        #         break
        #     else:
        #         poco("guide_view(Clone)").child("TxtContent").wait()
        #         logger.debug('等待GM窗口可用...')
        #         time.sleep(1)
        # poco("view_level_select_ui(Clone)").child("view_top_back_ui").child("BtnBackToMenu").wait()
        # logger.debug('尝试返回到主界面')
        # poco("view_level_select_ui(Clone)").child("view_top_back_ui").child("BtnBackToMenu").click()

        for i in range(3):
            if poco("home_view(Clone)").child("Top").child("SettingBtn").exists():
                logger.debug('已经进入主界面')
                UID = poco("home_view(Clone)").child("Top").child("RoleInfo").child("IDHead").child("IDLabel").get_text()
                logger.debug('当前账号的UID是%s'%UID)
                logger.debug('新账号解锁抽卡功能')
                commApi.sendgm(self, '~#unlockAllMinor')
                commApi.sendgm(self, '#unlockall -p 1')
                commApi.sendgm(self, '~#additem 75 100000')
                return UID
                break
            else:
                poco("uiroot_f1").child("UI").child("home_view(Clone)").child("Top").child("SettingBtn").wait()
                logger.debug('等待进入主界面...')
                time.sleep(1)

    def olduser_setup(self,user, pw):
        '''
        旧账号进入主界面的前置步骤
        '''
        if poco("view_login_ui(Clone)").child("bg").child("GameLogo").exists():
            logger.debug('成功进入游戏登录页面')
            pass
        else:
            poco("view_login_ui(Clone)").child("bg").child("GameLogo").wait()
            logger.debug('等待进入游戏登录页面中...')
        if poco("RememberMainPage").child("ExitLoginBtn").exists():
            logger.debug('当前登录页面记住了上次登陆的账号密码')
            poco("RememberMainPage").child("ExitLoginBtn").click()
            logger.debug('正在退出上次登录的账号中...')
            time.sleep(1)
            poco("decorationLayer(Clone)").child("dialogs").child(
                "message_2btn_dialog(Clone)").child("frame").child("layout").child("opt").child("sure_txt").click()
            logger.debug('成功退出上次登录的账号')
        else:
            logger.debug('当前登录页面没有记录上次的玩家登录信息')
            pass
        logger.debug('点击登录页面，尝试登录')
        poco("view_login_ui(Clone)").child("bg").child("BgBtn").click()
        poco("MainPage").child("Mask").child("Content").child("LoginPage").child("AccountLogin").child(
            "IdInput").click()
        logger.debug('账号输入中...')
        pyautogui.typewrite(user)
        poco("AccountLogin").child("PsdInput").click()
        logger.debug('密码输入中...')
        pyautogui.typewrite(pw)
        for i in range(5):
            logger.debug('尝试点击登录按钮进行登录')
            poco("LoginPage").child("AccountLogin").child("LoginBtn").child("LoginBtnRight").click()
            if poco("tips").child("message(Clone)").child("Content").exists():
                logger.debug('没有勾选是否同意隐私协议')
                poco("MainPage").child("Mask").child("Content").child(
                    "SecretToggleUp").child("Background").child("Checkmark").click()
                logger.debug('手动勾选隐私协议')
            else:
                logger.debug('隐私协议正常勾选状态，不需要再次勾选')
                break
        for i in range(3):
            if poco("home_view(Clone)").child("Top").child("SettingBtn").exists():
                logger.debug('已经进入主界面')
                UID = poco("home_view(Clone)").child("Top").child("RoleInfo").child("IDHead").child("IDLabel").get_text()
                logger.debug('当前账号的UID是%s'%UID)
                # logger.debug('新账号解锁抽卡功能')
                # commApi.sendgm(self, '~#unlockAllMinor')
                # commApi.sendgm(self, '#unlockall -p 1')
                # commApi.sendgm(self, '~#additem 75 100000')
                return UID
                break
            else:
                poco("uiroot_f1").child("UI").child("home_view(Clone)").child("Top").child("SettingBtn").wait()
                logger.debug('等待进入主界面...')
                time.sleep(1)


    def in_contract(self):
        '''
        进入付费招募页面
        '''
        logger.debug('尝试进入招募区二级页面')
        poco("home_view(Clone)").child("Main").child("Left").child("RecruitBtn").child("PartsGroup").click()
        poco("home_view(Clone)").child("StationRecruit").child("Right").child("PoolBtn").child("Title").wait()
        poco("home_view(Clone)").child("StationRecruit").child("Right").child("PoolBtn").child("Title").click()
        for i in range(5):
            logger.debug('尝试进入付费招募页面')
            if poco("view_recruit_pay_ui(Clone)").child("RecruitMain").child("Right").child("RecruitBtnOnce").exists():
                break
            else:
                logger.debug('等待付费招募页面加载中...')
                time.sleep(1)


    def newuser_contract(self):
        '''
        新账号抽卡前需要初始化，首先单抽抽掉新手引导的必抽部分
        '''

        logger.debug('尝试进入单抽页面')
        poco("view_recruit_pay_ui(Clone)").child("RecruitMain").child("Right").child("RecruitBtnOnce").click()
        for i in range(5):
            logger.debug('尝试进入转球页面')
            if poco("view_recruit_ui(Clone)").child("Description").exists():
                break
            else:
                logger.debug('等待转球页面加载中...')
                time.sleep(1)
        logger.debug('尝试开始转球球,开始前设置下阻尼和加速度')
        commApi().sendgm('#set acclimit -p 10000')
        commApi().sendgm('#set dampingradio -p 0.1')
        poco.swipe([0.3, 0.462], [0.83, 0.462], duration=0.05)

        poco("view_recruit_map_ui(Clone)").child("Skip").wait()
        logger.debug('尝试点击抽卡结果页的跳过...')
        poco("view_recruit_map_ui(Clone)").child("Skip").click()  # 抽卡跳过按钮
        logger.debug('尝试关闭舍友立绘...')
        poco("view_single_recruit_ui(Clone)").child("Frame").child("RoleCard").child("img").click()
        logger.debug('判断舍友获得后是否转化了道具...')
        if poco("view_common_get_items(Clone)").child("Description").child("Text").exists():
            logger.debug('当前舍友获得后有道具转化')
            poco("view_common_get_items(Clone)").child("Description").child("Text").click()
            logger.debug('关闭道具转化的获得页面')
        else:
            logger.debug('该舍友获得后没转化道具')
            pass

    def add_allrole(self):
        '''
        添加舍友专用GM，使用添加所有舍友后关闭弹出的舍友立绘
        '''
        poco("GMBar").child("BtnGm").click()
        poco("Cmd").child("InputCmd").click()
        pyautogui.typewrite('~#addAllRole')
        poco("Cmd").child("BtnSend").click()
        text=poco("decorationLayer(Clone)").child("view_gm_ui(Clone)").child("GMTools").child("Frame").child("Cmd").child("InputCmd").child("Text Area").child("Text").get_text()
        poco("GMBar").child("BtnGm").click()
        logger.info('输入GM命令完成，输入的GM命令是%s'%text.encode("utf-8",'ignore'))
        for i in range(20):
            if poco("view_single_recruit_ui(Clone)").child("Frame").child("RoleCard").child("img").exists():
                logger.debug('正在尝试关闭当前舍友立绘')
                poco("view_single_recruit_ui(Clone)").child("Frame").child("RoleCard").child("img").click()
                time.sleep(0.3)
            else:
                logger.debug('弹出的所有立绘均已关闭')
                break

    def back_home(self):
        for i in range(3):
            for x in range(6):
                if poco("BtnBack").exists():
                    logger.debug('尝试点击子页面的返回按钮')
                    poco("BtnBack").click()
                else:
                    break
            if poco("home_view(Clone)").child("Top").child("BackBtn").exists()==True:
                logger.debug('尝试点击主界面的返回按钮')
                poco("home_view(Clone)").child("Top").child("BackBtn").click()
            else:
                logger.debug('页面内已经没有返回按钮')
                break

    def get_excelcontent(self,excelname,column,row,sheet=0):
        '''
        获取项目配置表内的excel内容，需要填写表名
        column:填写key的取值行数(填写excel内的实际行数）
        row：填写velue的取值行数(填写excel内的实际行数）
        默认读取表里的第一个sheet的内容，如果要修改，则传对应excel里的sheet的数字（从0开始）
        '''
        pro_excelpath = 'F:\kleins_client\BuildDataConfig\DataConfig\Common\\'+excelname+'\\'+excelname+'.xlsx'
        excel_json = generalApi().openWorkbook(pro_excelpath, column, row,sheet)
        # logger.debug('当前sheet页为%s，内容为%s' % (sheet,excel_json))
        return excel_json

    def get_viocepath(self):
        '''
        获取播放语音后语音在项目中的路径，返回音频文件的名称
        '''
        poco("GMBar").child("BtnGm").click()
        poco("Cmd").child("InputCmd").click()
        pyautogui.typewrite('#audioShowVoicePath')
        poco("Cmd").child("BtnSend").click()
        total_count = poco("decorationLayer(Clone)").child("view_gm_ui(Clone)").child("GMTools").child(
            "Frame").child("Cmd").child("Scroll View").child("Viewport").child("Content").child(
            "CmdRecordLabel").get_text()
        poco("GMBar").child("BtnGm").click()
        voicepath = total_count.split(': ')[-2].split('\n')[-2]
        return voicepath

    def audio_recognition(self,voicepath):
        '''
        传入音频文件在项目中的路径（用GM：#audioShowVoicePath）获取到的路径
        return：音频文件解码后的语音文本
        '''

        change_audio = '../voice/' + voicepath.split('/')[-1] + '.wav'
        logger.debug('修改后的文件路径为%s' % change_audio)
        pro_voicepath = 'F:/kleins_client/Assets/RAW/f1_audio/audio/'
        source_audio = pro_voicepath + voicepath + '.ogg'
        logger.debug('尝试开始音频转码')
        execute_cmd_command(source_audio, change_audio)
        time.sleep(2)
        for req in range(3):
            try:
                logger.debug('尝试开始音频解析')
                api = RequestApi(appid="aaa", secret_key="aaa",
                                 upload_file_path=change_audio)
                voicedict = api.all_api_request()
                # commApi.back_home()
                data = voicedict['data']
                logger.debug('解析成功，解析内容为%s' % data)
                break
            except Exception as err:
                logger.error('科大讯飞接口不稳定，解析失败。需要重新请求%s' % err)
        list = []
        try:
            for d in eval(data):
                a = d['onebest']
                list.append(a)
            speak = ''.join(list)
            logger.debug('语音识别结果切割完成：%s' % speak)
            return speak
        except:
            logger.error('语音识别结果切割异常，内容是%s' % voicedict)

    def back_login(self):
        '''
        返回登录页面
        '''
        if poco("uiroot_f1").child("UI").child("view_login_ui(Clone)").exists():
            logger.debug('已经在登录页面，不需要进行退出登录操作')
        else:
            commApi().back_home()
            poco("home_view(Clone)").child("Top").child("SettingBtn").click()
            poco("view_setting_ui(Clone)").child("Left").child("Content").child("AccountBtn").click()
            logger.debug('尝试返回登录页面')
            poco("view_setting_ui(Clone)").child("Right").child("AccountPage").child("SwitchAccount").child("ShowBtn").click()
            poco("decorationLayer(Clone)").child("dialogs").child("message_2btn_dialog(Clone)").child("frame").child("layout").child("opt").child("sure_txt").click()
            for i in range(3):
                if poco("uiroot_f1").child("UI").child("view_login_ui(Clone)").exists():
                    logger.debug('已经返回登录页面，账号成功退出')
                    break
                else:
                    time.sleep(0.5)
                    logger.debug('还未返回登录页面')


