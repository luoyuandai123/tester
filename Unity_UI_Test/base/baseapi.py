# _*_coding:utf-8 _*_

import xlrd,json, hashlib,random

from airtest.core.api import *
from auto_ui.base.log import *
from poco.drivers.unity3d import UnityPoco
from poco.drivers.unity3d.device import UnityEditorWindow
from auto_ui.base.Old_kedaApi import *
from auto_ui.base.in_ffmpeg import *

os.environ['NLS_LANG'] = 'SIMPLIFIED CHINESE_CHINA.UTF8'#用于解决连接数据库乱码的问题
logger=log_set()



dev = UnityEditorWindow()
addr = ('', 5001)
poco = UnityPoco(addr, device=dev)



class generalApi():
    '''
    用来存放通用的方法，不与业务方法耦合
    '''

    def get_md5(self,src):
        '''
        传入字符串，转化为对应的md5值
        '''
        myMd5 = hashlib.md5()
        myMd5.update(src.encode("utf8"))
        myMd5_Digest = myMd5.hexdigest()
        return myMd5_Digest

    def now_timestamp(self):
        '''
        获取当前10位时间戳
        '''
        now = int(time.time())
        return now

    def now_time(self):
        '''
        获取当前时间，格式为%Y-%m-%d %H:%M:%S
        '''
        return str(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime()))

    def random_number(self):
        '''
        随机生成11位数字
        '''
        x = random.randint(20000000000, 99999999999)
        logger.debug('当前随机数为%s' % str(x))
        return str(x)

    def openWorkbook(self,excelFile,column,row,sheet=0):
        '''
        column:填写key的取值行数(填写excel内的实际行数）
        row：填写velue的取值行数(填写excel内的实际行数）
        默认读取表里的第一个sheet的内容，如果要修改，则传对应excel里的sheet的数字（从0开始）
        '''
        # 读取excel表的数据
        # excelFile = u'F:\kleins_client\BuildDataConfig\DataConfig\Common\wp_物品信息\wp_物品信息.xlsx'
        workbook = xlrd.open_workbook(excelFile)
        # 选取需要读取数据的那一页
        sheet = workbook.sheet_by_index(int(sheet))
        # 获得行数和列数
        rows = sheet.nrows
        cols = sheet.ncols
        # 创建一个数组用来存储excel中的数据
        p = []
        row=int(row)-1
        column=int(column)-1
        for i in range(row, rows):
            d = {}
            for j in range(0, cols):
                if len('%s' % sheet.cell(column, j).value)==0:
                    pass
                else:
                    q = '%s' % sheet.cell(column, j).value
                    d[q] = sheet.cell(i, j).value
            ap = []
            for k, v in d.items():
                if isinstance(v, float):  # excel中的值默认是float,需要进行判断处理，通过'"%s":%d'，'"%s":"%s"'格式化数组
                    ap.append('"%s":%d' % (k, v))
                else:
                    ap.append('"%s":"%s"' % (k, v))
            s = '{%s}' % (','.join(ap))  # 继续格式化
            p.append(s)
        t = '[%s]' % (','.join(p))  # 格式化
        data = json.dumps(t, ensure_ascii=False)
        excel_json=data.replace('\\','')[1:-1]
        x = json.loads(excel_json)
        workbook.release_resources()
        return x
        # with open('student4.json',"w",encoding='utf-8') as f:
        #     f.write(t)
        # openWorkbook()

