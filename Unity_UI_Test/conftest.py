# _*_coding:utf-8 _*_
""""
# @Time　　:2021/7/11 22:32
# @Author　 : king
# @File　　  :conftest.py
# @Software  :PyCharm
# @blog     :https://blog.csdn.net/u010454117
# @WeChat Official Account: 【测试之路笔记】
"""
from time import strftime
import time,requests,json
from py._xmlgen import html
import pytest

def pytest_collection_modifyitems(items):
    # item表示每个测试用例，解决用例名称中文显示问题
    for item in items:
        item.name = item.name.encode("utf-8").decode("unicode_escape")
        print('11111111111%s'%item.name)
        item._nodeid = item.nodeid.encode("utf-8").decode("unicode_escape")
        print('222222222222222222%s'%item._nodeid)

@pytest.mark.optionalhook
def pytest_html_results_table_header(cells):
    cells.insert(1, html.th('用例描述', class_="sortable", col="name"))  # 表头添加Description
    cells.insert(4, html.th('执行时间', class_='sortable time', col='time'))
    cells.pop(-1)  # 删除link

@pytest.mark.optionalhook
def pytest_html_results_table_row(report, cells):
    cells.insert(1, html.td(report.description))  # 表头对应的内容
    cells.insert(4, html.td(strftime('%Y-%m-%d %H:%M:%S'), class_='col-time'))
    cells.pop(-1)  # 删除link列

@pytest.mark.optionalhook
def pytest_html_results_table_html(report, data):   # 清除执行成功的用例logs
    if report.passed:
        del data[:]
        data.append(html.div('正常通过用例不抓取日志', class_='empty log'))

@pytest.mark.optionalhook
def pytest_html_report_title(report):
    report.title = "游戏冒烟自动化测试报告"

# 修改Environment部分信息，配置测试报告环境信息
def pytest_configure(config):
    # 添加接口地址与项目名称
    config._metadata["项目名称"] = "Kleins"
    # config._metadata['当前环境'] = env
    # config._metadata['项目地址'] = plathost
    config._metadata['开始时间'] = strftime('%Y-%m-%d %H:%M:%S')
    # 删除Java_Home
    # config._metadata.pop("JAVA_HOME")
    # config._metadata.pop("Packages")
    # config._metadata.pop("Platform")
    # config._metadata.pop("Plugins")
    # config._metadata.pop("Python")

# 修改Summary部分的信息
def pytest_html_results_summary(prefix, summary, postfix):
    prefix.extend([html.p("所属部门: 技术部")])
    # prefix.extend([html.p("测试人员: Boom")])

@pytest.mark.hookwrapper
def pytest_runtest_makereport(item, call):
    outcome = yield
    report = outcome.get_result()
    if item.function.__doc__ is None:
        report.description = str(item.function.__name__)
    else:
        report.description = str(item.function.__doc__)
    report.nodeid = report.nodeid.encode("unicode_escape").decode("utf-8") # 设置编码显示中文


def pytest_terminal_summary(terminalreporter, exitstatus, config):
    u'''收集测试结果'''
    # now=time.strftime('%Y-%m-%d %H:%M:%S')
    total = terminalreporter._numcollected
    passed = len(terminalreporter.stats.get("passed", []))
    failed = len(terminalreporter.stats.get("failed", []))
    error = len(terminalreporter.stats.get("error", []))
    skipped = len(terminalreporter.stats.get("skipped", []))
    duration = time.time() - terminalreporter._sessionstarttime
    deselected = len(terminalreporter.stats.get("deselected", []))  # 过滤的用例数
    content = u'[Game Auto_Test Report]\t\n' \
              u'TestCase Amount:%s\t\n' \
              u'Run TestCase Amount:%s \t\n' \
              u'Run PASS Amount:%s \t\n' \
              u'Run FAILED Amount:%s \t\n' \
              u'Run ERROR Amount:%s \t\n' \
              u'Run SKIP Amount:%s \t\n' \
              u'Run PASS Rate:%.2f %%  \t\n' \
              u'Runing Time:%.2f sec \t\n' % (
              total, total - deselected, passed, failed, error, skipped, passed / (total - deselected) * 100, duration)
    print(content)
    '''下方为调用飞书webhook的方法，如果不对接飞书需要注释'''
    url = "https://open.feishu.cn/open-apis/bot/v2/hook/fafd6103-d4cf-4812-9c71-7f6672d5db63nnnnnnnnnnnnnnnnnnnnn"
    payload_message = {
        "msg_type": "text",
        "content": {
            "text": content

        }
    }
    headers = {
        'Content-Type': 'application/json'
    }
    response = requests.request("POST", url, headers=headers, data=json.dumps(payload_message))
    return (content)

